{
  description = "System configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-24.05";
    nix-data-ops = {
      url = "gitlab:jhart/nix-data-ops";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # nixpkgs.url = "github:nixos/nixpkgs?rev=03d521141a171b85b545a8cc046d73d6b288d823";
    # nixpkgs.url = "/home/knut/Shared/shared/Programming/nix/nixpkgs";
    # draaft.url = "git+http://jonne:8929/knut/draaft"; # ?branch=staging";

    nixos-cosmic = {
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    base16-schemes.flake = false;
    base16-schemes.url = "github:base16-project/base16-schemes";
    crane.url = "github:ipetkov/crane";
    ags.url = "github:Aylur/ags";
    flake-utils.url = "github:numtide/flake-utils";
    hardware.url = "github:nixos/nixos-hardware";
    home-manager.url = "github:nix-community/home-manager?branch=24.05";
    nil.url = "github:oxalica/nil";
    nix-colors.url = "github:misterio77/nix-colors";
    nixgl.url = "github:guibou/nixGL";
    nix-inspect.url = "github:bluskript/nix-inspect";
    # simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-23.05";
    snowfall-lib.url = "github:snowfallorg/lib";
    sops-nix.url = "github:Mic92/sops-nix";
    stylix.url = "github:danth/stylix";

    crane.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    # simple-nixos-mailserver.inputs.nixpkgs.follows = "nixpkgs";
    snowfall-lib.inputs.nixpkgs.follows = "nixpkgs";
    # snowfall-lib.inputs.flake-utils-plus.url = "github:fl42v/flake-utils-plus";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = inputs:
    inputs.snowfall-lib.mkFlake {
      channels-config = {
        allowUnfree = true;
        nvidia.acceptLicense = true;
        permittedInsecurePackages = [
          "python3.11-apache-airflow-2.7.3"
        ];
      };

      # You must provide our flake inputs to Snowfall Lib.
      inherit inputs;

      # The `src` must be the root of the flake. See configuration
      # in the next section for information on how you can move your
      # Nix files to a separate directory.
      src = ./.;

      overlays = with inputs; [
        rust-overlay.overlays.default
      ];

      # Themes
      # theme = "catppuccin-mocha";
      # theme = "bespin";
      # theme = "gruvbox-dark-medium";
      # theme = "gigavolt";
      # theme = "ayu-mirage";
      systems = {
        # Add modules to all NixOS systems.
        modules.nixos = with inputs; [
        ];

        # Host: Roger
        hosts.roger.modules = with inputs; [
          stylix.nixosModules.stylix
          sops-nix.nixosModules.sops
          # {
          # nix.settings = {
          # substituters = ["https://cosmic.cachix.org/"];
          # trusted-public-keys = ["cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="];
          #   };
          # }
          nixos-cosmic.nixosModules.default
        ];

        hosts.roger.specialArgs = {
          theme = "ayu-mirage";
        };

        # Host: Fiete
        hosts.fiete.modules = with inputs; [
          stylix.nixosModules.stylix
        ];
        hosts.fiete.specialArgs = {
          # theme = "gruvbox-dark-medium";
          theme = "catppuccin-mocha";
        };

        # Host: bente
        hosts.bente.modules = with inputs; [
          stylix.nixosModules.stylix
          sops-nix.nixosModules.sops
          nix-data-ops.nixosModules.nix-data-ops
        ];

        hosts.bente.specialArgs = {
          pkgs-stable = import inputs.nixpkgs-stable {
            config.allowUnfree = true;
          };
          theme = "catppuccin-mocha";
          # theme = "nord";
          hostname = "bente";
          domain = "jhartma.org";
        };
      };

      # Configure Snowfall Lib, all of these settings are optional.
      snowfall = {
        # Tell Snowfall Lib to look in the `./nix/` directory for your
        # Nix files.
        root = ./nix;

        # Choose a namespace to use for your flake's packages, library,
        # and overlays.
        namespace = "knut";

        # Add flake metadata that can be processed by tools like Snowfall Frost.
        meta = {
          # A slug to use in documentation when displaying things like file paths.
          name = "system_config";

          # A title to show for your flake, typically the name.
          title = "My system configuration";
        };
      };
    };
}
