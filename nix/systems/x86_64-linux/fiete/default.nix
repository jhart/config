{
  lib,
  pkgs,
  ...
}: {
  nix = {
    # package = pkgs.nixUnstable;
    sshServe.write = true;
    settings = {experimental-features = ["nix-command" "flakes"];};
    gc.automatic = true;
    gc.dates = "10:15";
    optimise = {
      automatic = true;
      dates = [
        "03:45"
      ];
    };
  };
  imports = [
    ./hardware-configuration.nix
    ./imports/stylix.nix
    ./imports/services.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  documentation.man.enable = false;

  # Hyprland
  services.xserver.xkb.layout = "de";
  services.xserver.xkb.options = "eurosign:e";
  # services.xserver.desktopManager.gnome.enable = true;
  # services.xserver = {
  # enable = true;
  # displayManager.gdm = {
  # enable = true;
  # wayland = true;
  # };
  # };

  hardware = {
    opengl.enable = true;
  };

  # hyprland
  programs.hyprland = {
    enable = true;
    # xwayland.enable = true;
  };

  environment.sessionVariables = {
    POLKIT_AUTH_AGENT = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
    GSETTINGS_SCHEMA_DIR = "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}/glib-2.0/schemas";
    XDG_SESSION_TYPE = "wayland";
    WLR_NO_HARDWARE_CURSORS = "1";
    NIXOS_OZONE_WL = "1";
    MOZ_ENABLE_WAYLAND = "1";
    SDL_VIDEODRIVER = "wayland";
    CLUTTER_BACKEND = "wayland";
    XDG_CURRENT_DESKTOP = "Hyprland";
    XDG_SESSION_DESKTOP = "Hyprland";
    GTK_USE_PORTAL = "1";
    NIXOS_XDG_OPEN_USE_PORTAL = "1";
  };

  # xdg = {
  #   autostart.enable = true;
  #   portal = {
  #     enable = true;
  #     extraPortals = [
  #       pkgs.xdg-desktop-portal
  #       pkgs.xdg-desktop-portal-gtk
  #     ];
  #   };
  # };

  # Modules
  knut = {
    base.enable = true;
  };

  # Enable networking
  networking.networkmanager.enable = true;

  powerManagement.enable = false;

  users.users.knut = {
    initialPassword = "knut";
    createHome = true;
    isNormalUser = true;
    description = "Jörg Hartmann";
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    shell = pkgs.fish;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
