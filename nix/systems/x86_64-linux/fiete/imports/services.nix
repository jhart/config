{
  config,
  pkgs,
  inputs,
  ...
}: {
  # Don't suspend on lid close
  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";
  services.gvfs.enable = true;

  services.locate.enable = true;

  services.qemuGuest.enable = true;
  services.spice-webdavd.enable = true;
  services.spice-vdagentd.enable = true;
  services.spice-autorandr.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };
}
