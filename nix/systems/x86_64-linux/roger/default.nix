{
  lib,
  pkgs,
  inputs,
  ...
}: {
  # Your configuration.
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  documentation.man.enable = false;

  system.nixos = {
    tags = ["gdm"];
    label = "roger";
  };

  imports = [
    inputs.hardware.nixosModules.common-cpu-intel
    inputs.hardware.nixosModules.common-pc-laptop
    ./hardware-configuration.nix
    ./imports/programs.nix
    ./imports/services.nix
    ./imports/sops.nix
    ./imports/stylix.nix
    ./imports/networking.nix
    ./imports/nfs-automount.nix
    # ./imports/tlp.nix
  ];

  specialisation = {
    # Cosmic.configuration = {
    #   services.desktopManager.cosmic.enable = true;
    #   services.displayManager.cosmic-greeter.enable = true;
    # };
    Plasma.configuration = {
      system.nixos.tags = ["plasma"];
      knut.desktops.gnome.enable = false;
      knut.desktops.budgie.enable = false;
      # services.xserver.enable = true;
      services.displayManager.sddm.enable = true;
      services.desktopManager.plasma6.enable = true;
    };
  };

  # My custom modules
  knut = {
    base.enable = true;
    tuxedo.enable = true;
    desktops.gnome.enable = lib.mkDefault true;
    # desktops.hyprland.enable = true;
    networking.firewall.enable = true;
    networking.wireguard_client.enable = true;
    networking.wireguard_client.peers.wg1 = {
      enable = true;
      subnet = "10.65.65";
      endpoint = "sec.jhartma.org";
    };
    graphics.nvidia.enable = true;
    virtualisation.docker.enable = true;
  };

  # Tuxedo client
  hardware.tuxedo-rs = {
    enable = true;
    tailor-gui.enable = true;
  };

  # Power management
  powerManagement.enable = lib.mkForce true;
  services.power-profiles-daemon.enable = true;

  # Softare packages
  environment.systemPackages = import ./imports/packages.nix {inherit pkgs;};

  # Screen sharing
  security.rtkit.enable = true;

  systemd.extraConfig = "DefaultLimitNOFILE=2048"; # defaults to 1024 if unset

  services.pipewire = {
    enable = lib.mkDefault true;
    pulse.enable = lib.mkDefault true;
  };

  users.users.knut = {
    isNormalUser = true;
    home = "/home/knut";
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    uid = 1000;
    shell = pkgs.fish;
    initialPassword = "knut";
  };

  users.users.eeri = {
    isNormalUser = true;
    home = "/home/eeri";
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    uid = 1001;
    shell = pkgs.fish;
    initialPassword = "guest";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
