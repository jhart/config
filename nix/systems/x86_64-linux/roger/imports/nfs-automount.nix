{
  pkgs,
  config,
  ...
}: {
  systemd.mounts = [
    {
      type = "nfs";
      mountConfig = {
        Options = "noatime";
      };
      what = "10.65.65.1:/shared";
      where = "/home/knut/Shared";
    }
  ];

  systemd.automounts = [
    {
      # wantedBy = ["multi-user.target"];
      after = ["network.target" "wg-quick-wg1.service"];
      requires = ["network.target" "wg-quick-wg1.service"];
      automountConfig = {
        TimeoutIdleSec = "600";
      };
      where = "/home/knut/Shared";
    }
  ];
}
