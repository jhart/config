{
  programs.java.enable = true;

  programs.fish = {enable = true;};

  programs.steam = {
    enable = true;
  };
}
