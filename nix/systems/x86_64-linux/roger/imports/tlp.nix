{
  pkgs,
  inputs,
  systems,
  system,
  outputs,
  host,
  ...
}: let
  theme = systems.${host}.specialArgs.theme;
in {
  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
      CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
      CPU_ENERGY_PERF_POLICY_ON_BAT = "power";
      CPU_MAX_PERF_ON_AC = "100";
      CPU_MAX_PERF_ON_BAT = "0";
      CPU_MIN_PERF_ON_AC = "0";
      CPU_MIN_PERF_ON_BAT = "0";
      CPU_BOOST_ON_AC = "1";
      CPU_BOOST_ON_BAT = "0";
      PLATFORM_PROFILE_ON_AC = "performance";
      PLATFORM_PROFILE_ON_BAT = "low-power";
      # Enable runtime power management
      RUNTIME_PM_ON_AC = "auto";
      RUNTIME_PM_ON_BAT = "auto";
      # Enable Wifi power save
      WIFI_PWR_ON_AC = "on";
      WIFI_PWR_ON_BAT = "on";
    };
  };
}
