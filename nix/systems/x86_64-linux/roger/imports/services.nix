{
  pkgs,
  config,
  ...
}: {
  # For samba
  services.gvfs.enable = true;

  # Energy saving options
  services.cpupower-gui.enable = true;
  services.auto-cpufreq.enable = false;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.xkb.layout = "de";
  services.xserver.xkb.options = "eurosign:e";

  # For mounting nfs
  services.rpcbind.enable = true;

  services.hardware.openrgb = {
    enable = true;
    package = pkgs.openrgb-with-all-plugins;
  };

  # Ollama
  services.ollama = {
    enable = true;
    loadModels = [
      deepseek-r1:70b
    ];
    acceleration = "cuda";
  };

  # Searching
  services.locate = {enable = true;};

  # Enable the OpenSSH daemon. Necessary for SOPS
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "no";
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = true;
  };

  # Enable touchpad support.
  services.libinput = {
    enable = true;
    touchpad.scrollMethod = "edge";
    touchpad.disableWhileTyping = true;
  };

  # Database
  services.mysql.enable = true;
  services.mysql.package = pkgs.mariadb;

  # Network discovery
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  services.rdnssd.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [pkgs.gutenprint pkgs.gutenprintBin];
}
