{
  # specify which sops file to use for the secrets
  sops.defaultSopsFile = ../secrets/secrets.yaml;
  sops.secrets = {
    cache_jonne_public_key = {owner = "knut";};
    cache_jonne_private_key = {owner = "knut";};
    nix_signing_key_knut = {mode = "777";};
    openssh_knut = {
      owner = "root";
      group = "root";
      mode = "0440";
    };
    password_email_joerg_jhartma_org = {mode = "777";};
    password_email_joerg_leipzig_uni = {mode = "777";};
    password_email_mailbox = {mode = "777";};
    password_email_gmx = {mode = "777";};
    wireguard_jonne_public_key = {};
    wireguard_jonne_private_key = {};
    wireguard_roger_public_key = {};
    wireguard_roger_private_key = {};
    gitlab_rsa = {};
    id_rsa = {};
  };
}
