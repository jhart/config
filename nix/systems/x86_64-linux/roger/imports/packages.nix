{pkgs}:
with pkgs; [
  arion
  alpaca
  libudev0-shim
  udev
  auto-cpufreq
  aspell
  aspellDicts.de
  aspellDicts.en
  autoPatchelfHook
  binutils
  bluez
  bluez-tools
  busybox
  cups
  cryptsetup
  cmake
  ctags
  cifs-utils
  cpufrequtils
  dhcpcd
  docker
  docker-compose
  findutils
  ffmpeg-full
  gcc
  glibc
  gnumake
  git
  gutenprint
  gutenprintBin
  glxinfo
  htop
  hunspell
  hunspellDicts.de_DE
  hunspellDicts.en_US
  himalaya
  imagemagick
  iputils
  iftop
  hwinfo
  jre
  killall
  lame
  lshw
  sysfsutils
  lvm2
  libnotify
  libreswan
  networkmanager_strongswan
  lm_sensors
  man
  mesa
  networkmanager-vpnc
  nixd
  nixglIntel
  nload
  ncurses5
  neofetch
  nfs-utils
  nix-index
  nixfmt-classic
  ntfs3g
  nodejs
  ncurses
  openssl
  openssh
  patchelf
  pdftk
  p7zip
  postgresql
  gparted
  pandoc
  pass
  # rdesktop
  rsync
  ksmbd-tools
  sshfs-fuse
  system-config-printer
  stdenv_32bit
  sops
  spice
  spice-vdagent
  spacevim
  texlive.combined.scheme-full
  texlab
  tcpdump
  tmux
  traceroute
  tree
  unrar
  vim
  vpnc
  vdirsyncer
  wget
  wpa_supplicant
  whois
  win-virtio
  # win-spice
  wireguard-tools
  xorg.xmodmap
  xorg.libXtst
  zip
  zsh
  zlib

  # KDE
  kdePackages.merkuro

  # Terminal file managers
  # yazi
  nnn

  # Own packages
  knut.vm_start

  knut.stata18

  # Rust CLI tools
  alacritty
  bat
  nushell
  broot # file manager
  # felix-fm # file manager
  lf # file manager
  macchina # system information
  dysk # disk usage
  # inlyne # markdown and html viewer
  dua # disk analyzer
  nushell
]
