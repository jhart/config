{hostname, ...}: {
  networking.useDHCP = false;
  # networking.interfaces.wlo1.useDHCP = true;
  # networking.hostName = hostname;
  networking.networkmanager.enable = true;

  networking.hosts = {
    "127.0.0.1" = ["localhost"];
    "::1" = ["localhost" "roger"];
    "127.0.0.2" = ["roger"];
    "192.168.178.41" = ["bente.local"];
    "192.168.178.25" = ["jonne.local" "mm.jonne.local" "git.jonne.local" "sec.jonne.local"];
    # Names when in wireguard
    "10.66.66.1" = [
      "cockpit.jonne.home"
      "code.jonne.home"
      "git.jonne.home"
      "jonne.home"
      "mm.jonne.home"
      "rstudio.jonne.home"
      "sshgit.jonne.home"
      "virt.jonne.home"
    ];
    "10.65.65.1" = [
      "*.bente.home"
      "bente.home"
      "cockpit.bente.home"
      "code.bente.home"
      "git.bente.home"
      "jupyter.bente.home"
      "miniofs.bente.home"
      "mm.bente.home"
      "nc.bente.home"
      "prometheus.bente.home"
      "rstudio.bente.home"
      "sshgit.bente.home"
      "virt.bente.home"
    ];
  };
}
