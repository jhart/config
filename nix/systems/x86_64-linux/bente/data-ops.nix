{
  pkgs,
  ports,
}: rec {
  # spark.master = {
  #   enable = true;
  #   bind = "0.0.0.0";
  #   port = ports.PORT_SPARK;
  #   port_ui = ports.PORT_SPARK_UI;
  #   openFirewall = true;
  # };

  # spark.worker = {
  #   enable = true;
  #   master = "0.0.0.0:${toString ports.PORT_SPARK}";
  #   openFirewall = true;
  # };

  # datahub = {
  #   enable = false;
  #   port = 5014;
  #   openFirewall = true;
  # };

  # airflow = {
  #   enable = true;
  #   dataDir = /home/knut/Dokumente/airflow;
  #   port = ports.PORT_AIRFLOW;
  #   openFirewall = true;
  # };

  # superset = {
  #   enable = true;
  #   port = ports.PORT_SUPERSET;
  #   openFirewall = true;
  # };

  minio = {
    enable = true;
    host = "0.0.0.0";
    port = ports.PORT_MINIO;
    port_ui = ports.PORT_MINIO_UI;
    dataDir = [/pool-bente/miniofs];
    openFirewall = true;
    rootCredentialsFile = pkgs.writeTextFile {
      name = "minio-credentials";
      text = ''
        MINIO_ROOT_USER=minio
        MINIO_ROOT_PASSWORD=miniorootpwd
      '';
    };
  };

  # jupyterhub = {
  #   enable = true;
  #   port = ports.PORT_JUPYTERHUB;
  #   openFirewall = true;
  #   packages = with pkgs.python311Packages; [
  #     pymupdf
  #     pikepdf
  #     pdfplumber
  #   ];
  # };

  rstudio-server = {
    enable = false;
    openFirewall = true;
    r-packages = with pkgs.rPackages; [
      EValue
      Hmisc
      arsenal
      cowplot
      crayon
      descr
      DescTools
      diagram
      dplyr
      extrafont
      finetune
      foreign
      fst
      furniture
      ggplot2
      ggthemes
      # gt
      gtsummary
      haven
      here
      ipw
      janitor
      jsonlite
      knitr
      labelled
      languageserver
      lavaan
      ltm
      mice
      naniar
      openxlsx
      openxlsx2
      pROC
      raster
      rlang
      rpart
      scales
      semTools
      sf
      sjPlot
      sjmisc
      skimr
      sp
      srvyr
      stargazer
      survey
      terra
      tidymodels
      tidytidbits
      tidyverse
      vctrs
      vip
      visdat
      weights
      xml2
      xts
    ];
  };
}
