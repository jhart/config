{
  # Snowfall Lib provides a customized `lib` instance with access to your flake's library
  # as well as the libraries available from your flake's inputs.
  lib,
  # An instance of `pkgs` with your overlays and packages applied is also available.
  pkgs,
  systems, # An attribute map of your defined hosts.
  host,
  config,
  ...
}: let
  ports = import ./ports.nix;
in {
  nix = {
    package = pkgs.nix;
    sshServe.write = true;
    settings = {experimental-features = ["nix-command" "flakes"];};
    gc.automatic = true;
    gc.dates = "10:15";
    optimise = {
      automatic = true;
      dates = [
        "03:45"
      ];
    };
  };

  imports = [
    ./hardware-configuration.nix
    ./imports/backup.nix
    ./imports/dns.nix
    ./imports/kubeflow.nix
    ./imports/networking.nix
    ./imports/nextcloud.nix
    ./imports/nfs.nix
    ./imports/nginx.nix
    ./imports/no-rgb.nix
    ./imports/services.nix
    ./imports/sops.nix
    ./imports/stylix.nix
    ./imports/systemd_mounts.nix
  ];

  # Custom modules
  knut = {
    base.enable = true;
    desktops.gnome.enable = lib.mkDefault true;
    networking.wireguard.server_dns = {
      enable = true;
      subnet = "10.65.65";
    };

    networking.wireguard_client.enable = false;
  };

  # Container
  containers.jhartma = import ./imports/container-jhartma.nix {inherit lib pkgs config;};

  # Keyboard
  services.xserver.xkb.layout = "de";

  # RDP
  services.gnome.gnome-remote-desktop.enable = true;
  # services.xserver.enable = true;
  # services.displayManager.sddm.enable = lib.mkDefault true;
  # services.xserver.desktopManager.plasma5.enable = true;

  services.xrdp.enable = true;
  services.xrdp.openFirewall = true;
  # services.xrdp.defaultWindowManager = "dbus-launch --exit-with-session startplasma-x11";

  # Modules
  nix-data-ops = import ./data-ops.nix {inherit ports pkgs;};

  # Steam
  programs.steam = {enable = true;};

  # GPG
  programs.gnupg.agent = {
    enable = true;
  };
  services.dbus.packages = [pkgs.gcr];

  hardware.opengl = {
    enable = true;
  };

  # Enable networking
  networking.networkmanager.enable = true;

  # Enable Sound
  services.pipewire = {
    enable = true;
    pulse.enable = true;
  };

  # Power management
  powerManagement.enable = false;

  programs.fish = {enable = true;};

  programs.ssh.startAgent = true;

  programs.direnv = {
    enable = true;
    silent = true;
    loadInNixShell = false;
    nix-direnv.enable = false;
    direnvrcExtra = ''
      echo "Loaded direnv!"
    '';
  };

  users.users.knut = {
    initialPassword = "knut";
    createHome = true;
    isNormalUser = true;
    description = "Jörg Hartmann";
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    shell = pkgs.fish;
    openssh.authorizedKeys = {
      keyFiles = [config.sops.secrets.openssh_knut.path];
    };
  };

  environment.systemPackages = import ./imports/packages.nix {
    inherit pkgs;
    pkgs-stable = systems.${host}.specialArgs.pkgs-stable;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
