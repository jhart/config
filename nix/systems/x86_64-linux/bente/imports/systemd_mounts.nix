{ ...
}: {
  systemd.mounts = [
    {
      type = "ext4";
      mountConfig = {
      };
      what = "/dev/disk/by-uuid/864b0067-8ec5-4f65-b894-c5c625baebfa";
      where = "/backup";
    }
  ];
  systemd.automounts = [
    {
      where = "/backup";
      wantedBy = [ "multi-user.target" ];
      # wantedBy = ["restic-backups-external_disk.service"]; # Mount if backup are enabled
      automountConfig = {
        TimeoutIdleSec = "600s";
      };
    }
  ];
}
