# https://jacobneplokh.com/how-to-setup-nextcloud-on-nixos/
{
  config,
  pkgs,
  username,
  lib,
  systems,
  host,
  ...
}: let
  dbuser = "nextcloud";
  dbpwd = lib.readFile config.sops.secrets.password_postgres.path;
  dbname = "nextcloud";
  hostname = "nc.jhartma.org";
in {
  environment.etc."nextcloud-admin-pass".text = "PWD";
  services.nextcloud = {
    enable = true;
    package = systems.${host}.specialArgs.pkgs-stable.nextcloud30;
    hostName = hostname;
    home = "/var/lib/nextcloud";

    extraAppsEnable = true;
    appstoreEnable = true;

    # Use HTTPS for links
    https = true;

    config = {
      # DB
      dbtype = "pgsql";
      dbuser = dbuser;
      dbpassFile = "${pkgs.writeText "${dbuser}" "${dbpwd}"}";
      dbname = dbname;

      # Admin user
      adminuser = "admin";
      adminpassFile = "${pkgs.writeText "nextcloud-admin-pas" "${dbpwd}"}";
    };

    settings = {
      # Further forces Nextcloud to use HTTPS
      overwriteProtocol = "https";
    };
  };

  # Enable PostgreSQL
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_15;

    # Ensure the database, user, and permissions always exist
    ensureDatabases = [dbname];
    ensureUsers = [
      {
        name = dbuser;
        ensureDBOwnership = true;
      }
    ];
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all all trust
      host all all 127.0.0.1/32 trust
      host all all ::1/128 trust
      host  all  all 0.0.0.0/0 md5
    '';
  };
}
