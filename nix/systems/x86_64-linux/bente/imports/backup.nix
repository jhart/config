{
  pkgs,
  config,
  ...
}: {
  services.restic.backups = {
    # external_disk = {
    #   user = "root";
    #   initialize = true;
    #   repository = "/backup";
    #   passwordFile = config.sops.secrets.restic_pwd.path;
    #   paths = ["/var/lib/nextcloud" "/var/lib/postgresql" "/pool-bente/nfs/shared"];
    #   timerConfig = {
    #     OnCalendar = "19:32";
    #     Persistent = true;
    #   };
    # };
  };
  environment.systemPackages = with pkgs; [restic];
}
