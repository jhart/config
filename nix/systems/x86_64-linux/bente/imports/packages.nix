{ pkgs-stable, pkgs, }:
with pkgs-stable;
[
  autoPatchelfHook
  bind
  binutils
  btop
  busybox
  cmake
  cryptsetup
  ctags
  dhcpcd
  docker
  docker-compose
  findutils
  gcc
  git
  gitui
  glances
  glibc
  gnumake
  gnupg
  himalaya
  htop
  iputils
  jq
  killall
  lego
  lm_sensors
  man
  ncurses5
  neofetch
  nixd
  nixfmt-classic
  nload
  nss
  openrgb-with-all-plugins
  openssh
  openssl
  pass
  patchelf
  pulseaudioFull
  pulseaudio-ctl
  qemu
  qemu-utils
  qemu_kvm
  quickemu
  rsync
  sops
  tcpdump
  testdisk
  traceroute
  tree
  unrar
  unzip
  vdirsyncer
  vim
  wget
  whois
  win-virtio
  wireguard-tools
  wpa_supplicant
  zip
  zlib
  zsh

  # ZFS Tools
  zfs
  zfsnap
  zfstools
  zfsbackup
  zfs-replicate
  zfs-autobackup
  zfs-prune-snapshots

  # Terminal file managers
  yazi
  nnn

  # For RStudio
  gdal
  geos
  proj

  # Rust CLI tools
  alacritty
  bat
  nushell
  broot # file manager
  # felix-fm # file manager
  lf # file manager
  macchina # system information
  dysk # disk usage
  # inlyne # markdown and html viewer
  dua # disk analyzer
  ranger
] ++ [
  # Custom packages
  pkgs.nixglIntel
  pkgs.knut.vm
  pkgs.knut.stata18
  # knut.apache-ballista
  # knut.apache-ballista-ui
  # knut.pyballista
  pkgs.knut.yazi-picker
]
