{
  config,
  pkgs,
  inputs,
  ...
}: let
  ports = import ../ports.nix;
in {
  # Don't suspend on lid close
  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  services.hardware.openrgb.enable = true;
  services.hardware.openrgb.motherboard = "amd";

  services.locate.enable = true;

  # Enable Atuin server
  # services.atuin = {
  #   enable = true;
  #   openRegistration = true;
  #   package = pkgs.atuin;
  #   port = ports.PORT_ATUIN;
  #   openFirewall = true;
  # };

  services.surrealdb = {
    enable = true;
    port = ports.PORT_SURREAL_DB;
    host = "0.0.0.0";
    # dbPath = "file:///home/knut/surrealdb/";
    # dbPath = "memory";
    dbPath = "surrealkv://var/lib/surrealdb";
    extraFlags = [
      # "--log trace"
      "--username"
      "root"
      "--password"
      "root"
      "--allow-all"
      # "--auth"
      # "rocksdb://var/lib/dota"
    ];
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    ports = [ ports.PORT_SSH ];
    settings.PermitRootLogin = "yes";
    settings.AllowUsers = [ "knut" ];
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };

  services.pcscd.enable = true; # for GPG
}
