{
  systems,
  host,
  config,
  ...
}: let
  ports = import ../ports.nix;
  email = "jhart@kommespaeter.de";
  domain = systems.${host}.specialArgs.domain;
in {
  services.nginx = {
    enable = true;
    package = systems.${host}.specialArgs.pkgs-stable.nginx;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # Hosts
    virtualHosts = {
      # Test OCI-Container
      "test.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:8000";
          extraConfig =
            # required when the target is also TLS server with multiple hosts
            "proxy_ssl_server_name on;"
            +
            # required when the server wants to use HTTP Authentication
            "proxy_pass_header Authorization;";
        };
      };

      # Dev
      "dev.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_DEV;
          proxyWebsockets = true;
        };
      };

      # Prometheus
      "prometheus.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_PROMETHEUS;
          proxyWebsockets = true;
        };
      };

      # Ballista
      "ballista.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_BALLISTA_UI;
          proxyWebsockets = true;
        };
      };

      # Cockpit
      "cockpit.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_COCKPIT;
          proxyWebsockets = true;
        };
      };

      # JupyterHub
      "jupyter.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_JUPYTERHUB;
          proxyWebsockets = true;
        };
      };

      # MinioFS
      "miniofs.bente.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_MINIO_UI;
          proxyWebsockets = true;
        };
      };

      # VSCode server
      "code.bente.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_VSCODE;
          proxyWebsockets = true;
        };
      };

      # Rstudio server
      "rstudio.bente.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_RSTUDIO;
          proxyWebsockets = true;
        };
      };
      "rstudio.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_RSTUDIO;
          proxyWebsockets = true;
        };
      };

      # GITLAB
      "git.bente.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_GITLAB;
          proxyWebsockets = true;
        };
      };

      # Nextcloud
      ${config.services.nextcloud.hostName} = {
        forceSSL = true;
        enableACME = true;
      };

      # Atuin
      "atuin.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_ATUIN;
          proxyWebsockets = true;
        };
      };

      # DOTA
      "dota.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:" + toString ports.PORT_DEV;
          proxyWebsockets = true;
        };
      };

    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = email;
  };
}
