{
  config,
  pkgs,
  lib,
  ...
}: {
  autoStart = true;

  config = {
    config,
    pkgs,
    lib,
    ...
  }: {
    system.stateVersion = "24.05";

    nixpkgs = {
      config = {
        allowUnfree = true;
        allowUnsupportedSystem = true;
        experimental-features = "nix-command flakes";
      };
    };

    boot.isContainer = true;

    environment.variables = {
    };

    services.surrealdb = {
      enable = true;
      host = "0.0.0.0";
      port = 8000;
      extraFlags = [
        "--username"
        "root"
        "--password"
        "root"
        "--allow-all"
        "--auth"
      ];
    };
    services.redis.servers = {
      jhartma = {
        enable = true;
        databases = 16;
        logLevel = "debug";
        port = 6379;
        bind = "0.0.0.0";
        extraParams = ["--protected-mode no"];
      };
    };

    networking.firewall = {
      enable = false;
      allowedTCPPorts = [8000 6379];
      allowedUDPPorts = [8000 6379];
    };
    networking.hostName = "jhartma";
    networking.useDHCP = false;
    # networking.useHostResolvConf = lib.mkForce false;

    # services.resolved.enable = true;
  };
}
