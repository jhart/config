{
  pkgs,
  inputs,
  systems,
  host,
  system,
  outputs,
  ...
}: let
  theme = systems.${host}.specialArgs.theme;
in {
  stylix.enable = true;
  stylix.image = ../wallpaper.png;
  stylix.polarity = "dark";
  stylix.base16Scheme = "${inputs.base16-schemes}/${theme}.yaml";
  stylix.homeManagerIntegration.followSystem = true;
  stylix.autoEnable = true;

  stylix.fonts = {
    serif = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Serif";
    };

    sansSerif = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Sans";
    };

    monospace = {
      package = pkgs.dejavu_fonts;
      name = "DejaVu Sans Mono";
    };

    emoji = {
      package = pkgs.noto-fonts-emoji;
      name = "Noto Color Emoji";
    };
  };
}
