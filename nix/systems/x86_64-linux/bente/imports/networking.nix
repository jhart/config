{
  pkgs,
  config,
  ...
}: let
  nfsPorts = [
    2049 # NFS
  ];
  ports = import ../ports.nix;
in {
  # To allow internet acces for containers
  # networking.nat.enable = true;
  # networking.nat.internalInterfaces = ["ve-+"];
  # networking.nat.externalInterface = "enp0s31f6";
  # networking.networkmanager.unmanaged = ["interface-name:ve-*"];

  # Host settings
  networking.hostName = "bente";
  networking.hostId = "e33e30b1"; # required for zfs
  networking.resolvconf.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowPing = true;
  networking.firewall.enable = true;
  networking.firewall = {
    allowedTCPPorts =
      [
        # Nginx listens here, these are exposed by the router to the internet
        ports.PORT_HTTP
        ports.PORT_HTTPS

        # When in VPN, are not exposed by the router
        ports.PORT_DEV
        ports.PORT_DEV2
        ports.PORT_SSH
        ports.PORT_VSCODE
        # ports.PORT_WIREGUARD
        ports.PORT_DOTA_BOOK
        ports.PORT_SPARK
        ports.PORT_SPARK_UI
        ports.PORT_MINIO
        ports.PORT_MINIO_UI

        3001
        53 # Wireguard DNS
        143 # IMAP STARTTLS
        465 # SMTP TLS
        587 # SMTP STARTTLS
        993 # Imap TLS
        2224 # Gitlab SSH
        3389 # Remote Desktop
        5930 # Spice Server
        5999
      ]
      ++ nfsPorts;
    allowedUDPPorts =
      [
        53 # Wireguard DNS
        5930 # Spice Server
        ports.PORT_WIREGUARD
        ports.PORT_SSH
      ]
      ++ nfsPorts;
  };
}
