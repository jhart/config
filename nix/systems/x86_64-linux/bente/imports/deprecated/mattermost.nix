{
  lib,
  config,
  systems,
  host,
  ...
}: let
  ports = import ../ports.nix;
in {
  services.mattermost = {
    enable = true;
    package = systems.${host}.specialArgs.pkgs-stable.mattermost;
    siteUrl = "https://mm.jhartma.org";
    host = "127.0.0.1";
    port = ports.PORT_MATTERMOST;
    localDatabaseUser = "mattermost";
    localDatabaseName = "mattermost";
    localDatabasePassword = lib.readFile config.sops.secrets.password_postgres.path;
  };
}
