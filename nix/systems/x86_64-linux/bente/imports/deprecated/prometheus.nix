{
  pkgs,
  config,
  ...
}: let
  ports = import ../ports.nix;
in {
  services.prometheus = {
    enable = true;
    port = ports.PORT_PROMETHEUS;
  };
}
