{config, ...}: {
  mailserver = {
    enable = true;
    fqdn = "email.jhartma.org";
    domains = ["jhartma.org" "email.jhartma.org"];
    openFirewall = true;
    # forwards = {
    #   "joerg@jhartma.org" = "laodizea@gmx.de";
    # };
    mailboxes = {
      All = {
        auto = "subscribe";
        specialUse = "All";
      };
      Drafts = {
        auto = "subscribe";
        specialUse = "Drafts";
      };
      Junk = {
        auto = "subscribe";
        specialUse = "Junk";
      };
      Sent = {
        auto = "subscribe";
        specialUse = "Sent";
      };
      Trash = {
        auto = "no";
        specialUse = "Trash";
      };
    };

    # A list of all login accounts. To create the password hashes, use
    # nix-shell -p mkpasswd --run 'mkpasswd -sm bcrypt'
    loginAccounts = {
      # joerg = {
      #   hashedPasswordFile = config.sops.secrets.password_admin_mail.path;
      #   aliases = ["admin@jhartma.org" "joerg@jhartma.org" "joerg@email.jhartma.org" ];
      # };
      "joerg@email.jhartma.org" = {
        hashedPasswordFile = config.sops.secrets.password_admin_mail.path;
        aliases = ["joerg@jhartma.org" "admin@jhartma.org" "spam@jhartma.org"];
      };
    };

    # Use Let's Encrypt certificates. Note that this needs to set up a stripped
    # down nginx and opens port 80.
    certificateScheme = "acme";
  };
}
