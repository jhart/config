{
  pkgs,
  lib,
  config,
  ...
}: {
  virtualisation.docker.enable = true;

  virtualisation.oci-containers = {
    backend = "docker";
    containers = {
      nextcloud = {
        image = "nextcloud:28.0.3-apache";
        ports = ["8080:80"];
        autoStart = true;
        environment = {
          POSTGRES_DB = "nextcloud";
          POSTGRES_USER = "nextcloud";
          POSTGRES_PASSWORD = lib.readFile config.sops.secrets.password_postgres.path;
          POSTGRES_HOST = "localhost:8089";
          NEXTCLOUD_TRUSTED_DOMAINS = "nc.jhartma.org localhost:8080 nc.bente.home";
        };
        volumes = [
          # "/var/nextcloud:/var/www/html"
        ];
      };
      nextcloud-postgresql = {
        image = "postgres:16.2";
        ports = ["8089:5432"];
        volumes = [
          "/pool-bente/nextcloud/db:/var/lib/postgresql/data"
        ];
        environment = {
          POSTGRES_USER = "nextcloud";
          POSTGRES_PASSWORD = lib.readFile config.sops.secrets.password_postgres.path;
        };
      };
    };
  };
}
