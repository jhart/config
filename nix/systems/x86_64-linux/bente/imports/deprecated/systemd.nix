{
  config,
  pkgs,
  inputs,
  ...
}: let
  ports = import ../ports.nix;
in {
  # systemd.services.dota-book = {
  #   wantedBy = [ "multi-user.target" ];
  #   description = "Dota book";
  #   serviceConfig = {
  #     Type = "simple";
  #     ExecStart = "/bin/sh -c '${pkgs.miniserve}/bin/miniserve --index /home/knut/Shared/shared/Programming/dota/dota_book/book/index.html --spa --port ${toString ports.PORT_DOTA_BOOK}'";
  #     ExecStop = "/bin/kill -2 $MAINPID";
  #   };
  # };

  # systemd.services.win10 = {
  #   wantedBy = [ "multi-user.target" ];
  #   description = "Win10 VM";
  #   path = with pkgs; [
  #     bash
  #     gawk
  #   ];
  #   serviceConfig = {
  #     Type = "notify";
  #     Environment = [
  #       "VERBOSE=1"
  #     ];
  #     # ExecStart = "${start}/bin/start";
  #     ExecStart = "/bin/sh -c '${pkgs.knut.vm}/bin/vm start windows-10'";
  #     ExecStop = "${pkgs.knut.vm}/bin/vm stop windows-10";
  #     # User = "knut";
  #     # Group = "users";
  #     RuntimeDirectory = "~";
  #     # StateDirectory = "win10";
  #   };
  # };
}
