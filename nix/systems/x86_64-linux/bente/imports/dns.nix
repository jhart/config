{
  host, # The host name for this home.
  systems,
  ...
}: {
  networking.extraHosts = "
		10.65.65.1 rstudio.${host}.home code.${host}.home git.${host}.home cockpit.${host}.home virt.${host}.home ${host}.home
	";
  services.dnsmasq = {
    enable = true;
    package = systems.${host}.specialArgs.pkgs-stable.dnsmasq;
    alwaysKeepRunning = true;
    resolveLocalQueries = true;
    settings = {
      server = ["85.214.73.63" "208.67.222.222" "62.141.58.13"];
      cache-size = 1000;
      interface = "wg0";
    };
  };
}
