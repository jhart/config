{
  pkgs,
  config,
  ...
}: {
  services.nfs.server = {
    enable = true;
    hostName = "bente.home";
    exports = ''
      /pool-bente/nfs          10.65.65.1/24(rw,nohide,insecure,no_subtree_check,anonuid=1000,anongid=100,fsid=0)
      /pool-bente/nfs/shared   10.65.65.1/24(rw,nohide,insecure,no_subtree_check,anonuid=1000,anongid=100)
    '';
  };
}
