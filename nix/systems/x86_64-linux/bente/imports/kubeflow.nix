# https://dagshub.com/blog/how-to-install-kubeflow-locally/
{
  lib,
  pkgs,
  inputs,
  home, # The home architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this home (eg. `x86_64-home`).
  format, # A normalized name for the home target (eg. `home`).
  virtual, # A boolean to determine whether this home is a virtual target using nixos-generators.
  host, # The host name for this home.
  config,
  systems,
  ...
}: {
  virtualisation.docker = {
    enable = true;
    daemon.settings = {
      data-root = "/pool-bente/docker";
    };
  };

  environment.systemPackages = with pkgs; [
    minikube
    kubectl
    kustomize
  ];
}
