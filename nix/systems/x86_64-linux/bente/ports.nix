{
  PORT_AIRFLOW = 8076;
  PORT_ATUIN = 7654;
  PORT_BALLISTA = 9013;
  PORT_BALLISTA_UI = 9014;
  PORT_COCKPIT = 9090;
  PORT_DEV = 3000; # Port for development
  PORT_DEV2 = 3100; # Port for development
  PORT_DOTA_BOOK = 9002;
  PORT_DOTA_DOCS = 9001;
  PORT_GITLAB = 8929;
  PORT_HADOOP = 9000;
  PORT_HDFS = 9870;
  PORT_HOMEPAGE = 4455;
  PORT_HTTP = 80;
  PORT_HTTPS = 443;
  PORT_JUPYTERHUB = 9833;
  PORT_MATTERMOST = 8065;
  PORT_MINIO = 9011;
  PORT_MINIO_UI = 9012;
  PORT_PROMETHEUS = 9341;
  PORT_RSTUDIO = 8787; # Don't change
  PORT_SPARK = 4040;
  PORT_SURREAL_DB = 8322;
  PORT_SPARK_UI = 4041;
  PORT_SUPERSET = 8088;
  PORT_SSH = 22;
  PORT_VSCODE = 8769;
  PORT_WIREGUARD = 9176;
}
