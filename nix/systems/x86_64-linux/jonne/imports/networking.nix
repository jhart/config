{
  pkgs,
  config,
  ...
}: {
  # To allow internet acces for containers
  networking.nat.enable = true;
  networking.nat.internalInterfaces = ["ve-+"];
  networking.nat.externalInterface = "enp0s31f6";
  networking.networkmanager.unmanaged = ["interface-name:ve-*"];

  # Host settings
  networking.hostName = "jonne";
  networking.resolvconf.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowPing = true;
  networking.firewall = {
    allowedTCPPorts = [
      22 # SSH
      53 # DNS
      80 # HTTP
      143 # IMAP STARTTLS
      443 # HTTPS
      465 # SMTP TLS
      587 # SMTP STARTTLS
      993 # Imap TLS
      2049 # NFS
      2224 # Gitlab SSH
      5930 # Spice Server
      8769 # OpenVSCode
      9176 # Wireguard
    ];
    allowedUDPPorts = [
      53
      9176
      2049 # NFS
      5930 # Spice Server
    ];
  };
}
