{config, ...}: let
  email = "jhart@kommespaeter.de";
  domain = "jhartma.org";
  PORT_COCKPIT = 9090;
  PORT_DOTA_DOCS = 9001;
  PORT_GITLAB = 8929;
  PORT_HOMEPAGE = 4455;
  PORT_LIBVIRT = 16514;
  PORT_MATTERMOST = 8065;
  PORT_RSTUDIO = 8787;
  PORT_SSH = 22;
  PORT_VSCODE = 8769;
  PORT_WIREGUARD = 9176;
in {
  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # Hosts
    virtualHosts = {
      # Test OCI-Container
      "test.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:8000";
          extraConfig =
            # required when the target is also TLS server with multiple hosts
            "proxy_ssl_server_name on;"
            +
            # required when the server wants to use HTTP Authentication
            "proxy_pass_header Authorization;";
        };
      };

      # Homepage
      "jonne.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_HOMEPAGE;
          proxyWebsockets = true;
        };
      };

      # Cockpit
      "cockpit.jonne.home" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_COCKPIT;
          proxyWebsockets = true;
        };
      };

      # Mattermost
      "mm.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_MATTERMOST;
          proxyWebsockets = true;
        };
      };

      "mm.jonne.local" = {
        enableACME = false;
        forceSSL = false;
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_MATTERMOST;
          proxyWebsockets = true;
        };
      };

      # VSCode server
      "code.jonne.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_VSCODE;
          proxyWebsockets = true;
        };
      };

      # Rstudio server
      "rstudio.jonne.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_RSTUDIO;
          proxyWebsockets = true;
        };
      };

      "jonne" = {
        locations."/" = {
          proxyPass = "http://localhost:8769";
          proxyWebsockets = true;
        };
      };

      # GITLAB
      "virt.jonne.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_LIBVIRT;
          proxyWebsockets = true;
        };
      };

      # GITLAB
      "git.jonne.home" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_GITLAB;
          proxyWebsockets = true;
        };
      };

      # SSH
      "ssh.${domain}" = {
        locations."/" = {
          proxyPass = "http://localhost:" + toString PORT_SSH;
        };
      };

      # Wireguard
      "sec.${domain}" = {
        locations."/" = {
          proxyPass = "http://10.100.0.1:" + toString PORT_WIREGUARD;
          proxyWebsockets = true;
        };
      };

      # Nextcloud
      "${config.services.nextcloud.hostName}" = {
        forceSSL = true;
        enableACME = true;
      };

      # CACHE
      "cache.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/".proxyPass = "http://${config.services.nix-serve.bindAddress}:${
          toString config.services.nix-serve.port
        }";
      };

      # Hydra CI Server
      "hydra.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {proxyPass = config.services.hydra.hydraURL;};
        extraConfig =
          # required when the target is also TLS server with multiple hosts
          "proxy_ssl_server_name on;"
          +
          # required when the server wants to use HTTP Authentication
          "proxy_pass_header Authorization;";
      };

      # Dota docs
      "dota-docs.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/".proxyPass = "http://localhost:" + toString PORT_DOTA_DOCS;
        extraConfig =
          # required when the target is also TLS server with multiple hosts
          "proxy_ssl_server_name on;"
          +
          # required when the server wants to use HTTP Authentication
          "proxy_pass_header Authorization;";
      };

      # DRAAFT
      "draaft-staging.${domain}" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {proxyPass = "http://draaft";};
        extraConfig =
          # required when the target is also TLS server with multiple hosts
          "proxy_ssl_server_name on;"
          +
          # required when the server wants to use HTTP Authentication
          "proxy_pass_header Authorization;";
      };

      # Mail Server
      "email.${domain}" = {
        enableACME = true;
        forceSSL = true;
        serverName = "email.jhartma.org";
      };
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = email;
  };
}
