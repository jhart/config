{
  pkgs,
  config,
  ...
}: {
  services.nfs.server.enable = true;
  services.nfs.server.hostName = "jonne.home";
  services.nfs.server.exports = ''
    /home/knut/Shared     10.66.66.1/24(rw,nohide,insecure,fsid=0,no_subtree_check,all_squash,anonuid=1002,anongid=100)
    /home/knut/Shared/iso 10.66.66.1/24(rw,nohide,insecure,no_subtree_check,all_squash,anonuid=1002,anongid=100)
  '';
}
