{pkgs, ...}: let
  domain = "jhartma.org";
  dyndns = "jhart.dnshome.de";
in {
  security.acme = {
    acceptTerms = true;
    useRoot = true; # not recommended for security reasons
    defaults = {
      group = "root";
      email = "jhart@kommespaeter.de";
      # extraDomainNames = [ "*.${dyndns}" ];
      dnsProvider = "dnshomede";
      credentialsFile = ./credentials.txt;
    };
    certs.${dyndns} = {
      domain = dyndns;
      dnsPropagationCheck = true;
      postRun = ''
        chmod 777 /var/lib/acme/${dyndns}/
        chmod 777 /var/lib/acme/${dyndns}/*
        systemctl restart traefik
      '';
    };
  };

  services.traefik = {
    enable = true;
    group = "docker";

    staticConfigOptions = {
      api.dashboard = true;
      api.insecure = true;

      providers.file = {watch = true;};

      entryPoints.http = {
        address = ":80";
        http.redirections.entrypoint = {
          to = "websecure";
          scheme = "https";
        };
      };
      entryPoints.websecure.address = ":443";
    };

    dynamicConfigOptions = {
      tls = {
        stores.default = {
          defaultCertificate = {
            certFile = "/var/lib/acme/${domain}/cert.pem";
            keyFile = "/var/lib/acme/${domain}/key.pem";
          };
        };

        certificates = [
          {
            certFile = "/var/lib/acme/${domain}/cert.pem";
            keyFile = "/var/lib/acme/${domain}/key.pem";
            stores = "default";
          }
        ];
      };

      http.routers = {
        test = {
          rule = "Host(`test.${domain}`)";
          service = "test";
          entryPoints = "websecure";
          tls = true;
        };
      };
      http.services.test.loadBalancer.servers = [{url = "http://localhost:8000";}];
    };
  };
}
