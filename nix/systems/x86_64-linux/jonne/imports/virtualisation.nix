{
  pkgs,
  config,
  ...
}: {
  virtualisation.docker.enable = true;

  virtualisation.oci-containers = {
    backend = "docker";
    containers = {
      gitlab = {
        image = "gitlab/gitlab-ee:latest";
        ports = ["8929:8929" "2224:22"];
        autoStart = true;
        extraOptions = ["--hostname=git.jonne.home" "--shm-size=256m"];
        environment = {
          GITLAB_OMNIBUS_CONFIG = ''
            external_url 'http://localhost:8929'
            gitlab_rails['gitlab_shell_ssh_port'] = 2224
          '';
        };
        volumes = [
          "/srv/gitlab/config:/etc/gitlab"
          "/srv/gitlab/logs:/var/log/gitlab"
          "/srv/gitlab/data:/var/opt/gitlab"
        ];
      };
    };
  };
}
