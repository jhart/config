{
  config,
  pkgs,
  ...
}: let
  email = "jhart@kommespaeter.de";
in {
  services.nix-serve = {
    enable = true;
    # package = pkgs.nix-serve-ng;
    secretKeyFile = config.sops.secrets.cache_jonne_private_key.path;
    extraParams = "";
  };

  # services.harmonia.enable = true;
  # FIXME: generate a public/private key pair like this:
  # $ nix-store --generate-binary-cache-key cache.yourdomain.tld-1 /var/lib/secrets/harmonia.secret /var/lib/secrets/harmonia.pub
  # services.harmonia.signKeyPath = config.sops.secrets.cache_jonne_private_key.path;
  # Example using sops-nix to store the signing key
  #services.harmonia.signKeyPath = config.sops.secrets.harmonia-key.path;
  #sops.secrets.harmonia-key = { };
}
