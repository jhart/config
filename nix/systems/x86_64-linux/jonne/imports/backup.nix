{
  pkgs,
  config,
  ...
}: {
  services.restic.backups = {
    external_disk = {
      user = "backups";
      initialize = true;
      repository = "/mnt/backups";
      passwordFile = "~/.restic_password";
      paths = ["/var/lib/nextcloud" "/var/lib/postgresql"];
      timerConfig = {
        OnCalendar = "21:00";
        Persistent = true;
      };
    };
  };
}
