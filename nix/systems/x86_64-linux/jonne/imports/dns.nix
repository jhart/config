{
  pkgs,
  config,
  ...
}: {
  networking.extraHosts = "
		10.66.66.1 rstudio.jonne.home code.jonne.home git.jonne.home cockpit.jonne.home virt.jonne.home jonne.home
	";
  services.dnsmasq = {
    enable = true;
    alwaysKeepRunning = true;
    settings = {
      server = ["85.214.73.63" "208.67.222.222" "62.141.58.13"];
      cache-size = 1000;
      interface = "wg0";
      # port=5353;
    };
  };
}
