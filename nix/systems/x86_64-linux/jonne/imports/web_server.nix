{
  services.static-web-server = {
    enable = true;
    listen = "[::]:9001";
    # root = "/home/knut/projects/dota/rust/target/doc/";
    root = "/home/knut/projects/dota_book/";
    configuration = {
      general = {
        directory-listing = true;
      };
      advanced = {
        # source = "/dota_cli/index.html";
      };
    };
  };
}
