{pkgs, ...}: {
  services.mattermost = {
    enable = true;
    package = pkgs.mattermost;
    siteUrl = "https://mm.jhartma.org";
    listenAddress = ":8065";
  };
}
