{pkgs}:
with pkgs; [
  autoPatchelfHook
  binutils
  bind
  busybox
  cryptsetup
  cmake
  ctags
  dhcpcd
  docker
  docker-compose
  findutils
  gcc
  glances
  glibc
  git
  gnupg
  gnumake
  helix
  htop
  himalaya
  iputils
  jq
  killall
  lego
  lm_sensors
  man
  nixd
  nixglIntel
  nss
  nload
  ncurses5
  neofetch
  nixfmt-classic
  openssl
  openssh
  patchelf
  pass
  qemu
  qemu_kvm
  qemu-utils
  quickemu
  rsync
  sops
  tcpdump
  traceroute
  testdisk
  tree
  unrar
  unzip
  vim
  vdirsyncer
  wget
  wpa_supplicant
  whois
  win-virtio
  wireguard-tools
  zip
  zsh
  zlib

  # Terminal file managers
  yazi
  nnn

  # Rust CLI tools
  alacritty
  bat
  nushell
  broot # file manager
  # felix-fm # file manager
  lf # file manager
  macchina # system information
  dysk # disk usage
  # inlyne # markdown and html viewer
  dua # disk analyzer
  ranger
]
