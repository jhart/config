# For hydra to clone into private git repositories, the gitlab ssh keys must be present in the
# /var/lib/hydra folder, the key must be passwordless, they must be owned by the user "hydra" and
# have mode 600. The, when creating jobsets, choose flake and enter the URI git+ssh://git@gitlab.com/draaft/monorepo
{...}: {
  services.hydra = {
    enable = true;
    hydraURL = "http://localhost:3000"; # externally visible URL
    notificationSender = "hydra@localhost"; # e-mail of hydra service
    # a standalone hydra will require you to unset the buildMachinesFiles list to avoid using a nonexistant /etc/nix/machines
    buildMachinesFiles = [];
    # you will probably also want, otherwise *everything* will be built from scratch
    extraEnv = {BUILD_DATE = "soon";};
    useSubstitutes = true;
    extraConfig = "	<dynamicruncommand>\n	  enable = 1\n	</dynamicruncommand>\n";
  };
}
