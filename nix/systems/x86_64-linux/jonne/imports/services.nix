{
  config,
  pkgs,
  inputs,
  ...
}: {
  # Don't suspend on lid close
  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  services.locate.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = false;

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };

  services.pcscd.enable = true; # for GPG
}
