{
  # specify which sops file to use for the secrets
  sops.defaultSopsFile = ../secrets/secrets.yaml;
  sops.secrets = {
    openssh_knut = {
      owner = "root";
      group = "root";
      mode = "0440";
    };
    password_admin = {};
    password_admin_mail = {};
    password_email_joerg_jhartma_org = {mode = "777";};
    password_email_joerg_leipzig_uni = {mode = "777";};
    wireguard_jonne_private_key = {};
    wireguard_jonne_public_key = {};
    wireguard_roger_private_key = {};
    wireguard_roger_public_key = {};
    wireguard_fairphone_public_key = {};
    wireguard_tablet_public_key = {};

    cache_jonne_private_key = {
      owner = "knut";
      # group = "admin";
      mode = "6000";
    };
    cache_jonne_public_key = {
      owner = "knut";
      # group = "admin";
      mode = "6000";
    };
  };
}
