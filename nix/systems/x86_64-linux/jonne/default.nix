{
  # Snowfall Lib provides a customized `lib` instance with access to your flake's library
  # as well as the libraries available from your flake's inputs.
  lib,
  # An instance of `pkgs` with your overlays and packages applied is also available.
  pkgs,
  # You also have access to your flake's inputs.
  inputs,
  # Additional metadata is provided by Snowfall Lib.
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  # All other arguments come from the system system.
  config,
  ...
}: {
  nix = {
    package = pkgs.nixUnstable;
    sshServe.write = true;
    settings = {experimental-features = ["nix-command" "flakes"];};
    gc.automatic = true;
    gc.dates = "10:15";
    optimise = {
      automatic = true;
      dates = [
        "03:45"
      ];
    };
  };
  imports = [
    ./hardware-configuration.nix
    ./imports/cache.nix
    ./imports/dns.nix
    ./imports/hydra.nix
    ./imports/mattermost.nix
    ./imports/networking.nix
    ./imports/nextcloud.nix
    ./imports/nfs.nix
    ./imports/nginx.nix
    ./imports/samba.nix
    ./imports/services.nix
    ./imports/sops.nix
    ./imports/stylix.nix
    ./imports/virtualisation.nix
    ./imports/web_server.nix
    # ./imports/mailserver.nix
    # ./imports/roundcube.nix
    # ./imports/backup.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  documentation.man.enable = false;

  # Modules
  knut = {
    networking.wireguard.server_dns.enable = true;
    networking.wireguard_client = {};
    vscode-server.enable = true;
    rstudio-server.enable = true;
    cockpit.enable = true;
    virtualisation.docker.enable = true;
    virtualisation.libvirt.enable = true;
  };

  # GPG
  programs.gnupg.agent = {
    enable = true;
    # pinentryPackage = "gtk2";
  };
  services.dbus.packages = [pkgs.gcr];

  hardware.opengl = {
    enable = true;
    # driSupport = true;
    # driSupport32Bit = true;
  };

  # Container
  containers.draaft = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.100.10";
    localAddress = "192.168.100.11";
    config = import ../../../container/draaft/default.nix {
      inherit pkgs;
      backend = inputs.draaft.packages.x86_64-linux.backend;
      frontend = inputs.draaft.packages.x86_64-linux.frontend;
    };
  };

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "de_DE.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.UTF-8";
    LC_IDENTIFICATION = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_NAME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_TELEPHONE = "de_DE.UTF-8";
    LC_TIME = "de_DE.UTF-8";
  };

  powerManagement.enable = false;

  # Configure console keymap
  console.keyMap = "de";

  programs.fish = {enable = true;};

  programs.ssh.startAgent = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.admin = {
    isNormalUser = true;
    description = "Jörg Hartmann";
    hashedPasswordFile = config.sops.secrets.password_admin.path;
    shell = pkgs.fish;
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    openssh.authorizedKeys = {
      keyFiles = [config.sops.secrets.openssh_knut.path];
    };
    packages = with pkgs; [firefox];
  };

  users.users.knut = {
    initialPassword = "knut";
    createHome = true;
    isNormalUser = true;
    description = "Jörg Hartmann";
    extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
    shell = pkgs.fish;
    openssh.authorizedKeys = {
      keyFiles = [config.sops.secrets.openssh_knut.path];
    };
  };

  environment.systemPackages = import ./imports/packages.nix {inherit pkgs;};

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
