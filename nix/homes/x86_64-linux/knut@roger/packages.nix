{
  pkgs,
  inputs,
  ...
}: {
  home.packages = with pkgs; [
    alejandra # nix code formatter
    alpaca
    cacert
    chromium
    devenv
    direnv
    geary
    gitui
    gnupg
    gopass
    gwenview
    helix
    insomnia
    libreoffice-fresh
    marksman # md-lsp
    nextcloud-client
    notmuch
    okular
    pkg-config
    polonium
    pulseaudio
    remmina
    sddm-kcm
    spectacle
    spotifywm
    tailor-gui
    teams-for-linux
    texstudio
    thunderbird
    v4l-utils
    virt-manager
    vlc
    x11_ssh_askpass
    zed-editor
    zlib

    knut.yazi-picker
  ];
}
