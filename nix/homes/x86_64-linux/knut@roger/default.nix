{
  inputs,
  systems,
  pkgs,
  ...
}: let
  theme = systems.roger.specialArgs.theme;
in {
  home.stateVersion = "24.05";

  imports = [
    ./packages.nix
    inputs.nix-colors.homeManagerModule
    inputs.ags.homeManagerModules.default
  ];

  programs = import ./programs.nix;

  # stylix.base16Scheme = "${inputs.base16-schemes}/${theme}.yaml";
  stylix.base16Scheme = "${pkgs.base16-schemes}/share/themes/${theme}.yaml";
  stylix.targets.kde.enable = false;
  stylix.targets.gnome.enable = true;
  stylix.targets.btop.enable = true;
  stylix.autoEnable = true;

  knut = {
    hyprland.enable = true;

    git-client = {
      enable = true;
      userName = "jhart";
      userEmail = "services.jhart@mailbox.org";
    };

    editors.helix.enable = true;
    editors.vscode.enable = true;
    editors.lazyvim.enable = true;

    calendars.nextcloud.enable = true;
    shells.fish.enable = true;
    email.clients.enable = true;
    email.accounts.leipzig.enable = false;
    # email.accounts.draaft.enable = true;
    email.accounts.gmx.enable = true;
    # email.accounts.google.enable = true;
    # email.accounts.gwdg.enable = true;
    # email.accounts.jhartma.enable = true;
    email.accounts.mailbox.enable = true;
    # email.accounts.volt.enable = true;
  };
}
