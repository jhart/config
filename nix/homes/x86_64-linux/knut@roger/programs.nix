{
  home-manager = {
    enable = true;
  };

  gpg = {
    enable = true;
  };

  firefox = {
    enable = true;
  };

  zoxide = {
    enable = true;
    enableFishIntegration = true;
    options = [
      "--cmd cd"
    ];
  };

  zellij = {
    enable = false;
    settings = {
      on_force_close = "detach";
      simplified_ui = true;
      pane_frames = false;
      copy_on_select = true;
      copy_command = "wl-copy";
    };
  };

  direnv = {
    enable = true;
    #programs.direnv.nix-direnv.enable = true;
    enableZshIntegration = true;
  };

  git = {
    enable = true;
    userEmail = "services.jhart@mailbox.org";
    userName = "jhart";

    extraConfig = {
      init = {defaultBranch = "main";};
      pull = {rebase = true;};
      push = {autoSetupRemote = true;};
      core = {whitespace = "trailing-space,space-before-tab";};
    };
  };

  ssh = {
    enable = true;
    matchBlocks = {
      "git.bente.home" = {
        host = "git.bente.home";
        hostname = "git.bente.home";
        port = 2224;
        identityFile = "~/.ssh/gitlab_rsa";
        extraOptions = {
          "PreferredAuthentications" = "publickey";
        };
      };

      "gitlab.com" = {
        host = "gitlab.com";
        identityFile = "~/.ssh/gitlab_rsa";
        extraOptions = {
          "PreferredAuthentications" = "publickey";
        };
      };

      "jhart.gitlab.com" = {
        host = "jhart.gitlab.com";
        hostname = "gitlab.com";
        identityFile = "~/.ssh/gitlab_rsa";
        extraOptions = {
          "PreferredAuthentications" = "publickey";
        };
      };

      "github.com" = {
        hostname = "github.com";
        identityFile = "~/.ssh/github_ed25519.pub";
        user = "git";
      };
    };
  };
}
