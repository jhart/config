{
  home-manager = {
    enable = true;
  };

  gpg = {
    enable = true;
  };

  zoxide = {
    enable = true;
    enableFishIntegration = true;
    options = [
      "--cmd cd"
    ];
  };

  zellij = {
    enable = true;
    # enableFishIntegration = true;
    settings = {
      on_force_close = "detach";
      pane_frames = false;
    };
  };

  git = {
    enable = true;
    userEmail = "services.jhart@mailbox.org";
    userName = "jhart";
    extraConfig = {
      init = {defaultBranch = "main";};
      pull = {rebase = true;};
      push = {autoSetupRemote = true;};
      core = {whitespace = "trailing-space,space-before-tab";};
    };
  };
}
