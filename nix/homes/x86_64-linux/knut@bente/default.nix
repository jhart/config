{
  lib,
  pkgs,
  inputs,
  home, # The home architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this home (eg. `x86_64-home`).
  format, # A normalized name for the home target (eg. `home`).
  virtual, # A boolean to determine whether this home is a virtual target using nixos-generators.
  host, # The host name for this home.
  config,
  systems,
  ...
}: let
  theme = systems.${host}.specialArgs.theme;
in {
  home.stateVersion = "24.05";

  imports = [
    inputs.nix-colors.homeManagerModule
  ];

  programs = import ./programs.nix;

  stylix.base16Scheme = "${inputs.base16-schemes}/${theme}.yaml";
  stylix.targets.kde.enable = false;
  stylix.autoEnable = true;
  stylix.targets.btop.enable = true;

  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
  };

  knut = {
    atuin.enable = false;
    git-client = {
      enable = true;
      userName = "jhart";
      userEmail = "services.jhart@mailbox.org";
    };
    editors.helix.enable = true;
    editors.lazyvim.enable = true;
    editors.vscode.enable = true;
    calendars.nextcloud.enable = true;
    shells.fish.enable = true;
    email.clients.enable = true;
    email.accounts.leipzig.enable = true;
    email.accounts.draaft.enable = true;
    email.accounts.gmx.enable = true;
    email.accounts.google.enable = true;
    email.accounts.gwdg.enable = true;
    email.accounts.jhartma.enable = true;
    email.accounts.mailbox.enable = true;
    email.accounts.volt.enable = true;
  };

  home.packages = with pkgs; [
    firefox
    nextcloud-client
  ];
}
