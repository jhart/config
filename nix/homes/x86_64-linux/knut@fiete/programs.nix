{
  home-manager = {
    enable = true;
  };

  gpg = {
    enable = true;
  };

  firefox = {
    enable = true;
  };

  git = {
    enable = true;
    userEmail = "services.jhart@mailbox.org";
    userName = "jhart";

    extraConfig = {
      init = {defaultBranch = "main";};
      pull = {rebase = true;};
      push = {autoSetupRemote = true;};
      core = {whitespace = "trailing-space,space-before-tab";};
    };
  };
}
