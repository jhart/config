{
  pkgs,
  inputs,
  ...
}: {
  home.packages = with pkgs; [
    alejandra # nix code formatter
    bitwarden-cli
    cacert
    chromium
    direnv
    doctl
    glibc
    geary
    gnupg
    gopass
    gwenview
    helix
    insomnia
    jupyter-all
    kate
    kazam
    krdc
    ktorrent
    inputs.nil.packages.x86_64-linux.nil
    # libreoffice
    # libreoffice-fresh-unwrapped
    libreoffice-still
    nextcloud-client
    notmuch
    okular
    pkg-config
    pulseaudio
    sddm-kcm
    signal-desktop
    spectacle
    spotifywm
    spyder # python data analysis IDE
    texstudio
    thunderbird
    typst
    v4l-utils
    virt-manager
    vlc
    x11_ssh_askpass
    zlib
  ];
}
