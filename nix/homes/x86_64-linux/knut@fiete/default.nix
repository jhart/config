{
  lib,
  pkgs,
  inputs,
  home, # The home architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this home (eg. `x86_64-home`).
  format, # A normalized name for the home target (eg. `home`).
  virtual, # A boolean to determine whether this home is a virtual target using nixos-generators.
  host, # The host name for this home.
  config,
  systems,
  ...
}: let
  theme = systems.fiete.specialArgs.theme;
in {
  home.stateVersion = "24.05";

  imports = [
    ./packages.nix
    inputs.nix-colors.homeManagerModule
  ];

  programs = import ./programs.nix;

  wayland.windowManager.hyprland = {
    enable = true;
    systemd.enable = true;
  };

  # services.hyprpaper = {
  #   enable = true;
  # };

  stylix.base16Scheme = "${inputs.base16-schemes}/${theme}.yaml";
  stylix.targets.kde.enable = false;

  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnsupportedSystem = true;
      experimental-features = "nix-command flakes";
    };
  };

  knut = {
    git-client = {
      enable = true;
      userName = "jhart";
      userEmail = "services.jhart@mailbox.org";
    };
    shells.fish.enable = true;
  };
}
