{
  home-manager = {
    enable = true;
  };
  
  gpg = {
    enable = true;
  };

  git = {
    enable = true;
    userEmail = "services.jhart@mailbox.org";
    userName = "jhart";
    extraConfig = {
      init = {defaultBranch = "main";};
      pull = {rebase = true;};
      push = {autoSetupRemote = true;};
      core = {whitespace = "trailing-space,space-before-tab";};
    };
  };

  # ssh = {
  #   enable = true;
  #   matchBlocks = {
  #     "git.jonne.home" = {
  #       host = "git.jonne.home";
  #       hostname = "git.jonne.home";
  #       port = 2224;
  #       identityFile = "~/.ssh/gitlab_rsa";
  #       extraOptions = {
  #         "PreferredAuthentications" = "publickey";
  #       };
  #     };

  #     "gitlab.com" = {
  #       host = "gitlab.com";
  #       identityFile = "~/.ssh/gitlab_rsa";
  #       extraOptions = {
  #         "PreferredAuthentications" = "publickey";
  #       };
  #     };

  #     "jhart.gitlab.com" = {
  #       host = "jhart.gitlab.com";
  #       hostname = "gitlab.com";
  #       identityFile = "~/.ssh/gitlab_rsa";
  #       extraOptions = {
  #         "PreferredAuthentications" = "publickey";
  #       };
  #     };

  #     "github.com" = {
  #       hostname = "github.com";
  #       identityFile = "~/.ssh/id_rsa.pub";
  #       user = "git";
  #     };
  # };
  # };
}
