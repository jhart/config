{
  lib,
  inputs,
  pkgs,
  stdenv,
  ...
}: let
  script =
    pkgs.writeScript
    "vm_start"
    ''
      ${pkgs.openssh}/bin/ssh -fL 5999:localhost:5930 bente.home sleep 10; ${pkgs.spice-gtk}/bin/spicy -h 127.0.0.1 -p 5999
    '';
in
  stdenv.mkDerivation rec {
    name = "vm_start-${version}";
    version = "1.0.0";
    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/bin
      cd $out/bin
      cp -r ${script} vm_start
      chmod 777 vm_start
    '';

    meta = with lib; {
      description = "My scripts";
      platforms = with platforms; linux ++ darwin;
      license = licenses.mit;
    };
  }
# ssh -fL 5999:localhost:5930 jonne.home sleep 10; spicy -h 127.0.0.1 -p 5999

