{
  lib,
  inputs,
  pkgs,
  stdenv,
  ...
}: let
  script =
    pkgs.writeScript
    "yazi-picker"
    ''
      paths=$(yazi --chooser-file=/dev/stdout | while read -r; do printf "%q " "$REPLY"; done)

      if [[ -n "$paths" ]]; then
      	zellij action toggle-floating-panes
      	zellij action write 27 # send <Escape> key
      	zellij action write-chars ":open $paths"
      	zellij action write 13 # send <Enter> key
      	zellij action toggle-floating-panes
      fi

      zellij action close-pane
    '';
in
  stdenv.mkDerivation rec {
    name = "yazi-picker-${version}";
    version = "1.0.0";
    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/bin
      cd $out/bin
      cp -r ${script} yazi-picker
      chmod 777 yazi-picker
    '';

    meta = with lib; {
      description = "My scripts";
      platforms = with platforms; linux ++ darwin;
      license = licenses.mit;
    };
  }
