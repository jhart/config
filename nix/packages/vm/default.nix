{
  lib,
  inputs,
  pkgs,
  stdenv,
  ...
}: let
  vm = pkgs.writeScript "vm" ''
    # if [[ ! -n $2 ]];
    # then
    #   printf "\nError: Please provide a VM name, e.g. 'windows-10'!\n"
    #   exit 1
    # fi

    case $1 in
      start)
        printf "\nStarting Qemu with VM $2\n"
        cd /pool-bente/nfs/shared/machines
        ${pkgs.nixglIntel}/bin/nixGLIntel ${pkgs.quickemu}/bin/quickemu -vm /pool-bente/nfs/shared/machines/$2.conf --display spice
      ;;
      stop)
        printf "\nStopping VM $2\n"
        kill $(cat /pool-bente/nfs/shared/machines/$2/$2.pid)
      ;;
      *)
        printf "\n Please provide a 'start' or 'stop' argument!\n"
      ;;
    esac
  '';
in
  stdenv.mkDerivation rec {
    name = "vm-${version}";
    version = "1.0.0";
    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/bin
      cd $out/bin
      cp -r ${vm} vm
      chmod 777 vm
    '';

    meta = with lib; {
      description = "Starts the windows vm";
      platforms = with platforms; linux ++ darwin;
      license = licenses.mit;
    };
  }
