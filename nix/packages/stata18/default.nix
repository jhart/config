# https://nixos.wiki/wiki/Packaging/Binaries
# nix-build stata17.nix
# nix-env -i -f stata17.nix
{
  lib,
  inputs,
  pkgs,
  stdenv,
  ...
}:
stdenv.mkDerivation rec {
  name = "stata-${version}";
  version = "18";

  system = builtins.currentSystem;

  src = /home/knut/Software/Stata18;
  sourceRoot = ".";

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out/bin
    cp -R . $out/
    ls -al

    # symlink the binary to bin/
    ln -s $out/Stata18/stata $out/bin/stata
    ln -s $out/Stata18/xstata-mp $out/bin/xstata-mp
    ln -s $out/Stata18/stinit $out/bin/stinit
    ln -s $out/Stata18/stata.lic $out/bin/stata.lic
  '';
  preFixup = let
    # we prepare our library path in the let clause to avoid it become part of the input of mkDerivation
    libPath = with pkgs;
      lib.makeLibraryPath [
        stdenv.cc.cc.lib
        glib
        gtk2
        atk
        pango
        gdk-pixbuf
        cairo
        freetype
        fontconfig.lib
        libpng12
        zlib
        tlf
        gtk2
        gnome-themes-extra
        gtk3
        gtk3-x11
        adwaita-icon-theme
        ncurses5
        libadwaita
        curl
      ];
  in ''
    patchelf \
    --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
    --set-rpath "${libPath}" \
    $out/Stata18/xstata-mp \
    $out/Stata18/stata \
    $out/Stata18/stinit
  '';
  postPatch = ''
    # echo "POST PATCH"
  '';
  postFixup = ''
    echo "POST FIXUP Test"
  '';
  shellHook = ''
    export GTK_PATH="${pkgs.gtk2.out}/lib/gtk-2.0:${pkgs.gnome-themes-extra}/lib/gtk-2.0" \
  '';
  meta = with stdenv.glibc; {
    homepage = "https://www.stata.com/";
    description = "Data analysis";
    maintainers = ["jhart"];
  };
}
