# https://discourse.nixos.org/t/building-kernel-module/39466
# https://github.com/jordanisaacs/kernel-module-flake/blob/main/build/c-module.nix
{pkgs ? import <nixpkgs> {}}:
pkgs.stdenv.mkDerivation rec {
  name = "tuxedo-drivers-${version}-${kernel.version}";
  version = "4.11.4";

  src = pkgs.fetchFromGitHub {
    owner = "tuxedocomputers";
    repo = "tuxedo-drivers";
    rev = "v${version}";
    sha256 = "sha256-MN3p51Y9yqHc69BK8Wu8NtW18DdRIyxEYjH+Tqn/SXk=";
  };

  kernel = pkgs.linuxPackages_latest.kernel;

  nativeBuildInputs = kernel.moduleBuildDependencies;
  hardeningDisable = ["pic" "format"]; # 1
  buildInputs = [pkgs.nukeReferences kernel.dev];

  makeFlags = [
    "KERNELRELEASE=${kernel.modDirVersion}"
    "KDIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    "INSTALL_MOD_PATH=$out"
  ];

  installFlags = ["INSTALL_MOD_PATH=${placeholder "out"}"];

  postInstall = ''
    find $out/lib/modules/${kernel.modDirVersion} -name "*.ko" -exec xz {} \;
  '';

  meta.platforms = ["x86_64-linux"];
}
