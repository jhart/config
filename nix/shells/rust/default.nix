{
  lib,
  inputs,
  # namespace,
  pkgs,
  mkShell,
  ...
}: let
  rustVersion =
    pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default);
  pkgs_common = with pkgs; [
    pkg-config
    openssh
    openssl
  ];
  libPath = with pkgs; lib.makeLibraryPath pkgs_common;
in
  mkShell {
    # Create your shell
    packages = with pkgs; [
    ];

    name = "rust-env";
    nativeBuildInputs = [pkgs.pkg-config];
    buildInputs = with pkgs; [
      (rustVersion.override {
        extensions = ["rust-src"];
        targets = ["wasm32-unknown-unknown"];
      })
      bacon
      cargo
      cargo-generate
      cargo-leptos
      clippy
      rust-analyzer
      rustc
      rustfmt
      just
      httpie
      pkgs_common
    ];
    shellHook = ''
      export PATH="/run/current-system/sw/bin:/home/knut/.cargo/bin:/home/knut/.nix-profile/bin:$PATH";
      # export PATH="/run/current-system/sw/bin:/home/knut/.nix-profile/bin:$PATH";
      fish
    '';

    RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;
    PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
    LD_LIBRARY_PATH = libPath;
    GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
  }
