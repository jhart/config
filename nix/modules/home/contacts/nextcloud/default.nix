{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.contacts.nextcloud;
in {
  options.knut.contacts.nextcloud = {
    enable = mkOption {
      default = false;
      description = "Setup nextcloud calendar";
    };
  };

  config = mkIf cfg.enable {
    accounts.contact.accounts.nextcloud = {
      name = "Nextcloud";
      khal.enable = true;
      remote.type = "caldav";
      remote.url = "https://nextcloud.jhartma.org/remote.php/dav/addressbooks/users/jhartma/contacts/";
      remote.userName = "jhartma";
      remote.passwordCommand = ["pass Nextcloud"];
    };
  };
}
