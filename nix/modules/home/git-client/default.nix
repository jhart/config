{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.git-client;
in {
  options.knut.git-client = {
    enable = mkOption {
      default = false;
      description = "Enable git config";
    };
    userEmail = mkOption {
      default = "";
      description = "Git user email";
      type = types.str;
    };
    userName = mkOption {
      default = "";
      description = "Git username";
      type = types.str;
    };
  };

  config = mkIf cfg.enable {
    programs.gpg = {
      enable = true;
    };

    programs.git = {
      enable = true;
      userEmail = cfg.userEmail;
      userName = cfg.userName;

      extraConfig = {
        init = {defaultBranch = "main";};
        pull = {rebase = true;};
        push = {autoSetupRemote = true;};
        core = {whitespace = "trailing-space,space-before-tab";};
      };
    };

    programs.ssh = {
      enable = true;
      matchBlocks = {
        "bente.home" = {
          host = "bente.home";
          identityFile = "~/.ssh/id_rsa";
          extraOptions = {
            "PreferredAuthentications" = "publickey";
          };
        };
        "git.bente.home" = {
          host = "git.bente.home";
          hostname = "git.bente.home";
          port = 2224;
          identityFile = "~/.ssh/gitlab_rsa";
          # identityFile = inputs.sops-nix.secrets.gitlab_rsa.path;
          extraOptions = {
            "PreferredAuthentications" = "publickey";
          };
        };

        "gitlab.com" = {
          host = "gitlab.com";
          identityFile = "~/.ssh/gitlab_rsa";
          # identityFile = config.sops.secrets.gitlab_rsa.path;
          extraOptions = {
            "PreferredAuthentications" = "publickey";
            "ForwardAgent" = "yes";
          };
        };

        "jhart.gitlab.com" = {
          host = "jhart.gitlab.com";
          hostname = "gitlab.com";
          identityFile = "~/.ssh/gitlab_rsa";
          # identityFile = config.sops.secrets.gitlab_rsa.path;
          extraOptions = {
            "PreferredAuthentications" = "publickey";
            "ForwardAgent" = "yes";
          };
        };

        "github.com" = {
          hostname = "github.com";
          identityFile = "~/.ssh/github_ed25519.pub";
          # identityFile = config.sops.secrets.id_rsa.path;
          user = "git";
        };
      };
    };
  };
}
