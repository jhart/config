{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.atuin;
in {
  options.knut.atuin = {
    enable = mkOption {
      default = false;
      description = "Setup atuin";
    };
  };

  config = mkIf cfg.enable {
    programs.atuin = {
      enable = true;
      enableFishIntegration = true;
      package = pkgs.atuin;
      settings = {
        auto_sync = true;
        sync_frequency = "5min";
        sync_address = "https://atuin.jhartma.org";
      };
    };
  };
}
