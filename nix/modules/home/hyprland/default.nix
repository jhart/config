{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.hyprland;
  wallpaper = toString ./wallpaper.png;
in {
  options.knut.hyprland = {
    enable = mkOption {
      default = false;
      description = "Setup hyprland";
    };
  };

  config = mkIf cfg.enable {
    programs.wofi.enable = true;

    # programs.ags = {
    #   enable = true;

    #   extraPackages = with pkgs; [
    #     gtksourceview
    #     webkitgtk
    #     accountsservice
    #   ];
    # };

    services.hyprpaper = {
      enable = false;
      settings = {
        ipc = "on";
        splash = false;
        splash_offset = 2.0;

        preload = [wallpaper];

        wallpaper = [
          "eDP-1,${wallpaper}"
        ];
      };
    };
  };
}
