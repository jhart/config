{
  lib,
  pkgs,
  inputs,
  system,
  # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual,
  # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.clients;
in {
  options.knut.email.clients = {
    enable = mkOption {
      default = false;
      description = "Setup email accounts and programs";
    };
  };

  config = mkIf cfg.enable {
    programs = {
      # EMAIL
      mbsync = {enable = true;};
      msmtp.enable = true;

      # notmuch = {
      #   enable = true;
      #   hooks = {preNew = "mbsync --all";};
      # };

      # https://jonathanh.co.uk/blog/mutt-setup/
      neomutt = {
        enable = true;
        sidebar.enable = true;
        editor = "nvim";
        sort = "date-received";
        extraConfig = ''
          set sort = threads
           set sort_aux = reverse-last-date-received
            set date_format='%y/%m/%d %I:%M%p' '';
      };

      password-store = {
        enable = true;
        package = pkgs.pass;
      };

      offlineimap = {enable = true;};
      # Interact directly with imap accounts online

      himalaya = {
        enable = true;
        settings = {
          # backend = "imap";
          #   # display-name = "Jörg Hartmann";
          #   downloads-dir = "~/.config/himalaya/downloads";
          # signature = "";
        };
      };

      # aerc = {
      #   enable = true;
      #   extraConfig.general.unsafe-accounts-conf = true;
      # };
    };

    accounts.email = {
      # login to pass via: pass init hartmann.jrg@googlemail.com
      maildirBasePath = ".mail";
    };
  };
}
