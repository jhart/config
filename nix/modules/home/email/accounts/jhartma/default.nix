{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.jhartma;
  email = "joerg@email.jhartma.org";
  domain = "jhartma.org";
  host = "email.jhartma.org";
  imap-port = 143;
  smtp-port = 587;
in {
  options.knut.email.accounts.jhartma = {
    enable = mkOption {
      default = false;
      description = "Setup jhartma.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # jhartma.org
      accounts.jhartma = {
        address = email;
        userName = "joerg";
        realName = "Jörg Hartmann";
        imap = {
          host = host;
          port = imap-port;
        };
        smtp = {
          host = host;
          port = smtp-port;
        };
        neomutt = {
          enable = true;
          extraConfig = "color status cyan default";
          mailboxName = domain;
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        himalaya = {
          enable = true;
          settings = {
            backend = "imap";
            imap-host = host;
            imap-port = imap-port;
            imap-login = email;
            imap-auth = "passwd";
            imap-passwd = {
              cmd = "cat /run/secrets/password_email_joerg_jhartma_org";
            };
            imap-starttls = true;
            sender = "smtp";
            smtp-host = host;
            smtp-port = smtp-port;
            smtp-login = email;
            smtp-auth = "passwd";
            smtp-passwd = {
              cmd = "cat /run/secrets/password_email_joerg_jhartma_org";
            };
            smtp-starttls = true;
          };
        };
        thunderbird.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "cat /run/secrets/password_email_joerg_jhartma_org";
        offlineimap.enable = true;
      };
    };
  };
}
