{
  lib,
  pkgs,
  inputs,
  system,
  # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual,
  # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.mailbox;
  email = "joerghartmann@mailbox.org";
  domain = "mailbox.org";
in {
  options.knut.email.accounts.mailbox = {
    enable = mkOption {
      default = false;
      description = "Setup ${domain} account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # Mailbox.org
      accounts.${domain} = {
        primary = true;
        address = email;
        userName = email;
        realName = "Jörg Hartmann";
        imap = {
          host = "imap.${domain}";
          port = 993;
        };
        smtp = {
          host = "smtp.${domain}";
          port = 465;
        };
        neomutt = {
          enable = true;
          extraConfig = "color status cyan default";
          mailboxName = domain;
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        # aerc = {enable = true;};
        # himalaya = {
        #   enable = true;
        #   settings = {
        #     backend.type = "imap";
        #     backend.host = "imap.${domain}";
        #     backend.port = 993;
        #     backend.login = email;
        #     backend.auth.type = "password";
        #     backend.auth.cmd = "${pkgs.pass}/bin/pass Email/mailbox.org";
        # message.send = {
        #   backend = {
        #     type = "smtp";
        #     host = smtp;
        #     port = smtp_port;
        #     encryption = "start-tls";
        #     login = username;
        #     auth.type = "password";
        #     auth.cmd = "cat /run/secrets/password_email_joerg_leipzig_uni";
        #   };
        # };
        #   };
        # };
        thunderbird.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/mailbox.org";
        offlineimap.enable = true;
      };
    };
  };
}
