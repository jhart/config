{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.google;
in {
  options.knut.email.accounts.google = {
    enable = mkOption {
      default = false;
      description = "Setup Google Mail account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # Google
      accounts.google = {
        address = "hartmann.jrg@googlemail.com";
        userName = "hartmann.jrg@googlemail.com";
        realName = "Jörg Hartmann";
        imap = {
          host = "imap.gmail.com";
          port = 993;
          tls.enable = true;
        };
        smtp = {
          host = "smtp.gmail.com";
          port = 465;
        };
        neomutt = {
          enable = true;
          extraConfig = "color status cyan default";
          mailboxName = "Google";
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        # himalaya = {
        #   enable = true;
        # };
        msmtp.enable = true;
        thunderbird.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/gmail.com";
        offlineimap.enable = true;
      };
    };
  };
}
