{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.draaft;
in {
  options.knut.email.accounts.draaft = {
    enable = mkOption {
      default = false;
      description = "Setup Mailbox.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # Draaft
      accounts.draaft = {
        address = "info@draaft.co";
        userName = "info@draaft.co";
        realName = "Jörg Hartmann";
        imap = {
          host = "imap.udag.de";
          port = 993;
        };
        smtp = {
          host = "smtp.udag.de";
          port = 587;
        };
        neomutt = {
          enable = true;
          mailboxName = "Draaft";
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        # himalaya.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/draaft";
        offlineimap.enable = true;
      };
    };
  };
}
