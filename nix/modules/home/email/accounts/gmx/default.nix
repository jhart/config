{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.gmx;
in {
  options.knut.email.accounts.gmx = {
    enable = mkOption {
      default = false;
      description = "Setup Mailbox.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # GMX
      accounts.gmx = {
        address = "joerghartmann@gmx.org";
        userName = "joerghartmann@gmx.org";
        realName = "Jörg Hartmann";
        imap = {
          host = "imap.gmx.net";
          port = 993;
        };
        smtp = {
          host = "mail.gmx.net";
          port = 465;
        };
        # aerc.enable = true;
        neomutt = {
          enable = true;
          mailboxName = "GMX";
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        # himalaya = {
        #   enable = true;
        # };
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/gmx.net";
        offlineimap.enable = true;
      };
    };
  };
}
