{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.volt;
in {
  options.knut.email.accounts.volt = {
    enable = mkOption {
      default = false;
      description = "Setup Mailbox.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      # Volt
      accounts.volt = {
        address = "joerg.hartmann@volteuropa.org";
        userName = "joerg.hartmann@volteuropa.org";
        realName = "Jörg Hartmann";
        imap = {
          host = "imap.gmail.com";
          port = 993;
        };
        smtp = {
          host = "smtp.gmail.com";
          port = 465;
        };
        neomutt = {
          enable = true;
          mailboxName = "Volt";
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        # himalaya.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/volt";
        offlineimap.enable = true;
      };
    };
  };
}
