{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.gwdg;
in {
  options.knut.email.accounts.gwdg = {
    enable = mkOption {
      default = false;
      description = "Setup Mailbox.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      accounts.gwdg = {
        address = "joerg.hartmann@sowi.uni-goettingen.de";
        userName = "gwdg/jhartma";
        realName = "Jörg Hartmann";
        # flavor = "plain";
        imap = {
          host = "email.gwdg.de";
          port = 993;
          tls.enable = true;
          tls.useStartTls = false;
        };
        smtp = {
          host = "email.gwdg.de";
          port = 587;
        };
        folders = {
          inbox = "INBOX";
          sent = "Sent";
          trash = "Trash";
        };
        neomutt = {
          enable = true;
          mailboxName = "UniGoettingen";
        };
        mbsync = {
          enable = true;
          create = "maildir";
          extraConfig.account = {
            "AuthMechs" = "PLAIN";
            "PipelineDepth" = "1";
          };
          extraConfig.remote = {"MaxSize" = "500m";};
        };
        # himalaya = {
        #   enable = true;
        #   settings = {
        #     default = true;
        #   };
        # };
        msmtp.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/gwdg.de";
        offlineimap.enable = true;
      };
    };
  };
}
