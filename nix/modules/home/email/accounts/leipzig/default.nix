{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.email.accounts.leipzig;
  email = "joerg.hartmann@uni-leipzig.de";
  username = "wb50hihu";
  imap = "imap.uni-leipzig.de";
  imap_port = 143;
  smtp = "smtp.uni-leipzig.de";
  smtp_port = 25;
in {
  options.knut.email.accounts.leipzig = {
    enable = mkOption {
      default = false;
      description = "Setup Mailbox.org account";
    };
  };

  config = mkIf cfg.enable {
    accounts.email = {
      accounts.leipzig = {
        address = email;
        userName = username;
        realName = "Jörg Hartmann";
        imap = {
          host = imap;
          port = imap_port;
          tls.enable = true;
        };
        smtp = {
          host = smtp;
          port = smtp_port;
        };
        folders = {
          inbox = "INBOX";
          sent = "Sent";
          trash = "Trash";
        };
        neomutt = {
          enable = true;
          mailboxName = "UniLeipzig";
        };
        mbsync = {
          enable = true;
          create = "maildir";
        };
        himalaya = {
          enable = true;
          settings = {
            default = true;
            backend.type = "imap";
            backend.host = imap;
            backend.port = imap_port;
            backend.login = username;
            backend.encryption = "start-tls";
            backend.auth.type = "password";
            backend.auth.cmd = "cat /run/secrets/password_email_joerg_leipzig_uni";

            message.send.backend.type = "smtp";
            message.send.backend.host = smtp;
            message.send.backend.port = smtp_port;
            message.send.backend.encryption = "start-tls";
            message.send.backend.login = username;
            message.send.backend.auth.type = "password";
            message.send.backend.auth.cmd = "cat /run/secrets/password_email_joerg_leipzig_uni";
          };
        };
        msmtp.enable = true;
        thunderbird.enable = true;
        notmuch.enable = true;
        passwordCommand = "${pkgs.pass}/bin/pass Email/UniLeipzig";
        offlineimap.enable = true;
      };
    };
  };
}
