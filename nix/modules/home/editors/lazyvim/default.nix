{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  cfg = config.knut.editors.lazyvim;
in {
  options.knut.editors.lazyvim = {
    enable = mkOption {
      default = false;
      description = "Setup lazyvim";
    };
  };

  config = mkIf cfg.enable {
    programs.neovim = {
      # enable = true;
      # plugins = with pkgs; [
      #   vimPlugins.LazyVim
      # ];
      # extraLuaConfig = ''
      #   require("config.lazy")
      # '';
    };
    # xdg.configFile.nvim = dot_files;
    xdg.configFile."nvim/lua/config/lazy.lua" = {
      source = ./nvim/lua/config/lazy.lua;
    };
    xdg.configFile."nvim/lua/config/autocmds.lua" = {
      source = ./nvim/lua/config/autocmds.lua;
    };
    xdg.configFile."nvim/lua/config/keymaps.lua" = {
      source = ./nvim/lua/config/keymaps.lua;
    };
    xdg.configFile."nvim/lua/config/options.lua" = {
      source = ./nvim/lua/config/options.lua;
    };
    xdg.configFile."nvim/lua/plugins/example.lua" = {
      source = ./nvim/lua/plugins/example.lua;
    };
    xdg.enable = true;

    home.packages = with pkgs; [
      ripgrep
      # neovim
      fd
      lazygit
      unzip
      tree-sitter
      wl-clipboard
      cargo
      lua-language-server
      stylua
      rustfmt
      # python311Packages.python-lsp-server
      rPackages.languageserver
    ];
  };
}
