{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  cfg = config.knut.editors.helix;
in {
  options.knut.editors.helix = {
    enable = mkOption {
      default = false;
      description = "Setup helix";
    };
  };

  config = mkIf cfg.enable {
    programs.helix = {
      enable = lib.mkForce true;
      settings = {
        # theme = "kaolin-valley-dark";
        editor = {
          bufferline = "always";
          cursorline = true;
          line-number = "relative";
          lsp.display-messages = true;
        };
        keys.normal = {
          space.F = "file_picker_in_current_directory";
          space.space = "file_picker_in_current_buffer_directory";
          space.w = ":w";
          space.q = ":q";
          esc = ["collapse_selection" "keep_primary_selection"];
          # space.p = ":sh zellij run -f -x 10% -y 10% --width 80% --height 80% -- bash yazi-picker";
        };
      };
      languages = {
        language = [
          {
            name = "nix";
            scope = "source.nix";
            injection-regex = "nix";
            shebangs = [];
            auto-format = true;
            roots = [];
            file-types = ["nix"];
            comment-token = "#";
            formatter = {command = "alejandra";};
            language-servers = ["nix-lsp"];
            indent = {
              tab-width = 2;
              unit = "  ";
            };
          }
          {
            name = "lua";
            injection-regex = "lua";
            auto-format = true;
            file-types = ["lua"];
            comment-token = "--";
            formatter = {command = "luaformatter";};
            language-servers = ["lua-language-server"];
          }
          {
            name = "latex";
            scope = "source.tex";
            injection-regex = "tex";
            file-types = ["tex"];
            roots = [];
            comment-token = "%";
            language-servers = ["texlab"];
            # config = {texlab = {build = {onSave = true;};};};
            indent = {
              tab-width = 4;
              unit = "\t";
            };
          }
        ];
        language-server.nix-lsp = {
          command = "nil";
        };
        language-server.lua-language-server = {
          command = "lua-language-server";
        };
      };
    };
    home.packages = with pkgs; [
      alejandra
      lua-language-server
      luaformatter
      knut.yazi-picker
    ];
  };
}
