{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.editors.vscode;
in {
  options.knut.editors.vscode = {
    enable = mkOption {
      default = false;
      description = "Setup vscode";
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      # vscodium-fhs
      (vscode-with-extensions.override {
        vscodeExtensions = with vscode-extensions;
          [
            # arcticicestudio.nord-visual-studio-code
            bbenoist.nix
            brettm12345.nixfmt-vscode
            catppuccin.catppuccin-vsc
            catppuccin.catppuccin-vsc-icons
            cweijan.vscode-database-client2
            github.codespaces
            github.copilot
            github.copilot-chat
            github.vscode-github-actions
            github.vscode-pull-request-github
            gitlab.gitlab-workflow
            gruntfuggly.todo-tree
            jdinhlife.gruvbox
            jnoortheen.nix-ide
            kamadorueda.alejandra
            ms-azuretools.vscode-docker
            # ms-toolsai.jupyter
            # ms-toolsai.jupyter-renderers
            # ms-toolsai.jupyter-keymap
            # ms-toolsai.vscode-jupyter-cell-tags
            # ms-toolsai.vscode-jupyter-slideshow
            ms-vscode-remote.remote-ssh
            mskelton.one-dark-theme
            rust-lang.rust-analyzer
            serayuzgur.crates
            shd101wyy.markdown-preview-enhanced
            skellock.just
            tamasfe.even-better-toml
          ]
          ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
          ];
      })
    ];
  };
}
