{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.shells.fish;
  theme_string = readFile ./theme_custom.toml;
  theme =
    replaceStrings
    [
      "base00"
      "base01"
      "base02"
      "base03"
      "base04"
      "base05"
      "base06"
      "base07"
      "base08"
      "base09"
      "base0A"
      "base0B"
      "base0C"
      "base0D"
      "base0E"
    ]
    [
      config.lib.stylix.colors.base00
      config.lib.stylix.colors.base01
      config.lib.stylix.colors.base02
      config.lib.stylix.colors.base03
      config.lib.stylix.colors.base04
      config.lib.stylix.colors.base05
      config.lib.stylix.colors.base06
      config.lib.stylix.colors.base07
      config.lib.stylix.colors.base08
      config.lib.stylix.colors.base09
      config.lib.stylix.colors.base0A
      config.lib.stylix.colors.base0B
      config.lib.stylix.colors.base0C
      config.lib.stylix.colors.base0D
      config.lib.stylix.colors.base0E
    ]
    theme_string;
in {
  options.knut.shells.fish = {
    enable = mkOption {
      default = false;
      description = "Enable fish shell";
    };
  };

  config = mkIf cfg.enable {
    programs.nix-index = {
      enable = true;
      enableFishIntegration = true;
    };

    programs.starship = {
      enable = true;
      enableFishIntegration = true;
      package = pkgs.starship;
    };

    # Fish plugins
    home.packages = with pkgs; [
      fishPlugins.pure
      fishPlugins.sponge
      fishPlugins.colored-man-pages
    ];

    # Starship config for custom prompt
    home.file.".config/starship.toml".text = theme;

    # Fish config
    programs.fish = {
      enable = true;
      # loginShellInit = "${pkgs.fastfetch}/bin/fastfetch";

      shellAbbrs = {
        c = "code .";
        sys = "sudo systemctl";
        # ls = "exa --git --long --header --icons";
        tr = "tree -C --dirsfirst -L 3";
        pro = "cd $HOME/Nextcloud/Documents/UniGoettingen/Projekte";
        con = "hx $HOME/Nextcloud/@system/flake.nix";
        app = "cd $HOME/Dokumente/Programming/Draaft/draaft-monorepo";
        dota = "cd $HOME/Dokumente/Programming/Projects/dota_project";
        syncSW = "nextcloudcmd --silent --user wb50hihu@uni-leipzig.de --password (pass UniLeipzig) /home/knut/Dokumente/UniLeipzig/ https://speicherwolke.uni-leipzig.de";
        syncNC = "nextcloudcmd --silent --user jhartma --password (pass Nextcloud)  /home/knut/Nextcloud/ https://nextcloud.jhartma.org";
        bente = "ssh bente.home";
      };
      interactiveShellInit = ''
        starship init fish | source
      '';
      shellAliases = {
        cpro = "$HOME/Nextcloud/Documents/UniGoettingen/Projekte";
      };
    };
  };
}
