{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.data-science.spark;
  HADOOP_HOME = "/var/lib/hadoop";
in {
  options.knut.data-science.spark = {
    enable = mkOption {
      default = false;
      description = "Enable Apache Spark";
    };
    port = mkOption {
      default = 7077;
      description = "Spark port";
    };
    port_ui = mkOption {
      default = 7078;
      description = "Spark UI port";
    };
    # port_httpfs = mkOption {
    #   default = 14000;
    #   description = "Hadoop HTTPFS port";
    # };
    # port_hdfs = mkOption {
    #   default = 9870;
    #   description = "dfs.namenode.http-address";
    # };
  };

  config = mkIf cfg.enable {
    services.spark = {
      master = {
        enable = true;
        bind = "0.0.0.0";
        extraEnvironment = {
          # SPARK_MASTER_OPTS = "-Dspark.deploy.defaultCores=5";
          SPARK_MASTER_WEBUI_PORT = toString cfg.port_ui;
          SPARK_MASTER_PORT = toString cfg.port;
        };
      };
      worker = {
        enable = true;
        master = "0.0.0.0:${toString cfg.port}";
      };
    };

    # services.hadoop = {
    #   coreSite = {
    #      "fs.defaultFS" = "hdfs://localhost:${toString cfg.port}";
    #   };
    #   hdfsSite = {
    #     "dfs.nameservices" = "namenode1";
    #     "dfs.replication" = "1";
    #     "dfs.namenode.name.dir" = "/pool-bente/hadoop/hdfs/namenode";
    #     "dfs.datanode.data.dir" = "/pool-bente/hadoop/hdfs/datanode";
    #   };
    #   httpfsSite = {
    #     "httpfs.http.port" = toString cfg.port_httpfs;
    #   };
    #   hdfsSiteDefault = {
    #     "dfs.namenode.http-address" = "0.0.0.0:${toString cfg.port_hdfs}";
    #     "dfs.namenode.http-bind-host" = "0.0.0.0";
    #     "dfs.namenode.rpc-bind-host" = "0.0.0.0";
    #     "dfs.namenode.servicerpc-bind-host" = "0.0.0.0";
    #   };
    #   hdfs = {
    #     namenode.enable = true;
    #     namenode.formatOnInit = true;
    #     httpfs.openFirewall = true;
    #     httpfs.enable = true;
    #     httpfs.tempPath = "/pool-bente/hadoop/httpfs/tmp";

    #   };
    # };

    environment.variables = {
      #   HADOOP_COMMON_HOME=HADOOP_HOME;
      #   HADOOP_COMMON_LIB_NATIVE_DIR="${HADOOP_HOME}/lib/native";
      #   HADOOP_HDFS_HOME=HADOOP_HOME;
      #   HADOOP_HOME=HADOOP_HOME;
      #   HADOOP_INSTALL=HADOOP_HOME;
      #   HADOOP_MAPRED_HOME=HADOOP_HOME;
      #   HADOOP_OPTS="-Djava.library.path=${HADOOP_HOME}/lib/native";
      #   PATH="$PATH:${HADOOP_HOME}/sbin:${HADOOP_HOME}/bin";
      #   YARN_HOME=HADOOP_HOME;

      JAVA_HOME = "${pkgs.jdk}";
      SPARK_HOME = "${pkgs.spark}";
      SPARK_DIST_CLASSPATH = "";
      PYSPARK_PYTHON = mkDefault "${pkgs.python3Packages.python}/bin/${pkgs.python3Packages.python.executable}";
      PYTHONPATH = mkDefault "$PYTHONPATH:$PYTHONPATH:${pkgs.python311Packages.py4j}";
      SPARKR_R_SHELL = "${pkgs.R}/bin/R";
    };

    environment.systemPackages = with pkgs; [
      # python311Packages.pyspark
      spark
      jdk
      python311Packages.py4j
      python311
      scala_2_12
      R
    ];
  };
}
