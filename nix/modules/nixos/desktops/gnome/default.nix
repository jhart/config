# from https://github.com/the-argus/nixsys/blob/main/modules/desktops/gnome.nix
{
  # config,
  # options,
  # pkgs,
  # lib,
  # Snowfall Lib provides a customized `lib` instance with access to your flake's library
  # as well as the libraries available from your flake's inputs.
  lib,
  # An instance of `pkgs` with your overlays and packages applied is also available.
  pkgs,
  # You also have access to your flake's inputs.
  inputs,
  # Additional metadata is provided by Snowfall Lib.
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual,
  # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  # All other arguments come from the module system.
  config,
  options,
  ...
}: let
  cfg = config.knut.desktops.gnome;
  inherit (lib) mkIf mkEnableOption;
in {
  options.knut.desktops.gnome = {
    enable = mkEnableOption "Gnome Desktop Environment";
    x11 = mkEnableOption "Use X11";
  };

  config = mkIf cfg.enable {
    services.xserver.enable = true;
    services.xserver.desktopManager.gnome.enable = true;
    services.xserver.displayManager.gdm.enable = lib.mkDefault true;
    services.xserver.displayManager.gdm.wayland =
      if cfg.x11
      then false
      else true;

    services.gnome.localsearch.enable = true;
    services.gnome.tinysparql.enable = true;
    services.gnome.gnome-keyring.enable = true;

    programs.dconf.enable = true;

    xdg.portal.extraPortals = with pkgs; [xdg-desktop-portal-gnome];

    environment.systemPackages = with pkgs; [
      # apostrophe # Markdown editor
      citations
      cartridges # Games launcher
      newsflash
      railway
      powersupply
      blackbox-terminal

      gnome-console
      gnome-photos
      gnome-secrets
      gnome-calendar
      gnome-disk-utility
      gnome-tweaks

      gnome-contacts
      gnome-logs
      gnome-notes
      gnome-remote-desktop

      gnomeExtensions.tiling-shell
      gnomeExtensions.tiling-assistant
      gnomeExtensions.vitals
      gtg
      planify
    ];
    services.udev.packages = with pkgs; [gnome-settings-daemon];
  };
}
