# from https://github.com/the-argus/nixsys/blob/main/modules/desktops/gnome.nix
{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}: let
  cfg = config.knut.desktops.hyprland;
  inherit (lib) mkIf mkEnableOption;
in {
  options.knut.desktops.hyprland = {
    enable = mkEnableOption "Hyprland";
  };

  config = mkIf cfg.enable {
    hardware = {
      graphics = {
        enable = true;
        # driSupport = true;
        # driSupport32Bit = true;
      };
    };

    services = {
      libinput.enable = true;
      xserver = {
        enable = true;
        xkb.layout = "de";
        xkb.variant = "";
        # videoDrivers = ["nvidia"];
        excludePackages = [pkgs.xterm];
        displayManager.gdm = {
          enable = lib.mkDefault true;
          wayland = true;
        };
      };
    };

    programs = {
      hyprland = {
        enable = true;
        xwayland = {
          enable = true;
        };
      };
      waybar = {
        enable = true;
        # package = pkgs.waybar.overrideAttrs (oldAttrs: {
        #   mesonFlags = oldAttrs.mesonFlags ++ ["-Dexperimental=true"];
        # });
      };
    };

    environment.systemPackages = with pkgs; [
      kitty
      alacritty
      # hyprpaper
      ags
      libdbusmenu-gtk3
    ];
  };
}
