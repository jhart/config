{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}: let
  cfg = config.knut.desktops.budgie;
  inherit (lib) mkIf mkEnableOption;
in {
  options.knut.desktops.budgie = {
    enable = mkEnableOption "Budgie Desktop Environment";
  };

  config = mkIf cfg.enable {
    services.xserver.enable = true;
    services.xserver.desktopManager.budgie.enable = true;
    services.xserver.displayManager.lightdm.enable = true;
    services.pipewire = {
      enable = lib.mkForce false;
      pulse.enable = lib.mkForce false;
    };
    hardware.pulseaudio.enable = lib.mkForce false;
  };
}
