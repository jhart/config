# from https://github.com/the-argus/nixsys/blob/main/modules/desktops/gnome.nix
{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}: let
  cfg = config.knut.desktops.plasma;
  inherit (lib) mkIf mkEnableOption;
in {
  options.knut.desktops.plasma = {
    enable = mkEnableOption "Plasma Desktop Environment";
  };

  config = mkIf cfg.enable {
    services.xserver.enable = true;
    services.xserver.desktopManager.plasma5.enable = true;
    services.displayManager.sddm.enable = lib.mkForce true;

    environment.plasma5.excludePackages = with pkgs.libsForQt5; [
      # oxygen
      # khelpcenter
      # discover
    ];
    environment.systemPackages = with pkgs; [
      # KDE Themes
      # kdeplasma-addons
      # kde-gruvbox
      # adapta-kde-theme
      # arc-kde-theme
      # adapta-kde-theme
      # materia-kde-theme
      # graphite-kde-theme
      # layan-kde
      # colloid-kde
      # qogir-kde
      # pitch-black
      # sweet-nova
      # utterly-nord-plasma
      # plasma-overdose-kde-theme
      # libsForQt5.breeze-qt5
      # libsForQt5.breeze-grub
      # libsForQt5.breeze-icons

      # KDE Apps
      # plasma5Packages.kdeconnect-kde
      # kdenlive
      # kleopatra
      # kmail
      # korganizer
      # kaddressbook

      # libsForQt5.kolourpaint
      # libsForQt5.filelight
      # libsForQt5.elisa
      # libsForQt5.kitinerary
      # libsForQt5.kwallet
      # libsForQt5.kwallet-pam
      # libsForQt5.kwalletmanager
      # libsForQt5.xdg-desktop-portal-kde
      # xdg-desktop-portal
      # libsForQt5.lightly

      # libsForQt5.akonadi-notes
      # libsForQt5.akonadi-mime
      # libsForQt5.akonadi-contacts
      # libsForQt5.akonadi-import-wizard
      # libsForQt5.akonadi-search
      # libsForQt5.akonadiconsole
      # libsForQt5.akonadi-calendar
    ];
  };
}
