{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.gitlab-server;
in {
  options.knut.gitlab-server = {
    enable = mkOption {
      default = false;
      description = "Enable gitlab server";
    };
  };

  config = mkIf cfg.enable {
    services.gitlab = rec {
      enable = true;
      host = "localhost";
      port = 4314;

      # You, dear sysadmin, have to make these files exist.
      initialRootPasswordFile = /home/admin/initial-password;
      initialRootEmail = "joerg@jhartma.org";

      secrets = rec {
        # A file containing 30 "0" characters.
        secretFile = config.sops.secrets.gitlab_secret.path;
        dbFile = "${config.sops.secrets.gitlab_dbfile.path}";
        otpFile = "${config.sops.secrets.gitlab_otpfile.path}";
        # openssl genrsa 2048 > jws.rsa
        jwsFile = /home/admin/.ssh/gitlab-jws.rsa;
      };
    };
  };
}
