{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.virtualisation.libvirt;
in {
  options.knut.virtualisation.libvirt = {
    enable = mkOption {
      default = false;
      description = "Enable Xen virtualisation";
    };
  };

  config = mkIf cfg.enable {
    system.nixos.tags = ["with-libvirt"];
    virtualisation.libvirtd = {
      enable = true;
      qemu.ovmf.enable = true;
      qemu.ovmf.packages = [pkgs.OVMFFull.fd];
    };
    # qemu.options = [
    #   # Better display option
    #   "-vga virtio"
    #   "-display gtk,zoom-to-fit=false"
    #   # Enable copy/paste
    #   # https://www.kraxel.org/blog/2021/05/qemu-cut-paste/
    #   "-chardev qemu-vdagent,id=ch1,name=vdagent,clipboard=on"
    #   "-device virtio-serial-pci"
    #   "-device virtserialport,chardev=ch1,id=ch1,name=com.redhat.spice.0"
    # ];
    # };
  };
}
