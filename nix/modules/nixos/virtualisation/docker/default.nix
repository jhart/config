{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.virtualisation.docker;
in {
  options.knut.virtualisation.docker = {
    enable = mkOption {
      default = false;
      description = "Enable docker virtualisation";
    };
  };

  config = mkIf cfg.enable {
    system.nixos.tags = ["with-docker"];
    virtualisation.docker = {
      enable = true;
      enableOnBoot = true;
      rootless.enable = true;
    };
  };
}
