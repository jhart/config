{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.glances-server;
in {
  options.knut.glances-server = {
    enable = mkOption {
      default = false;
      description = "Enable Glances server";
    };
  };

  config = mkIf cfg.enable {
    # TODO
  };
}
