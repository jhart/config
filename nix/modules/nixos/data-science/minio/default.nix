{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.data-science.miniofs;
in {
  options.knut.data-science.miniofs = {
    enable = mkOption {
      default = false;
      description = "Enable MinioFS";
    };
    port = mkOption {
      default = 9011;
      description = "Minio API port";
    };
    port_console = mkOption {
      default = 9012;
      description = "Minio UI port";
    };
  };

  config = let
    credentials = pkgs.writeTextFile {
      name = "minio-credentials";
      text = ''
        MINIO_ROOT_USER=minio
        MINIO_ROOT_PASSWORD=miniorootpwd
      '';
    };
  in
    mkIf cfg.enable {
      services.minio = {
        enable = true;
        dataDir = ["/pool-bente/miniofs"];
        listenAddress = ":${toString cfg.port}";
        consoleAddress = ":${toString cfg.port_console}";
        browser = true;
        rootCredentialsFile = credentials;
      };

      environment.systemPackages = with pkgs; [
        minio-client
      ];
      environment.sessionVariables = {
        MINIO_API_SELECT_PARQUET = "on";
      };
    };
}
