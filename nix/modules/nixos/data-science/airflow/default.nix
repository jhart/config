{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.data-science.airflow;
  dockerfile = builtins.toFile "airflow-docker" (builtins.replaceStrings ["8080:8080"] ["${toString cfg.port}:8080"] (lib.readFile ./docker-compose.yaml));
in {
  options.knut.data-science.airflow = {
    enable = mkOption {
      default = false;
      description = "Enable Apache Airflow Orchestration";
    };

    dir = mkOption {
      default = /tmp;
      description = "The directory where Airflow stores its data";
    };

    uid = mkOption {
      default = 1000;
      description = "UID of the user";
    };

    port = mkOption {
      default = 8080;
      description = "Port of the web interface";
    };

    openFirewall = mkOption {
      default = false;
      description = lib.mdDoc ''Opens the firewall for Apache Airflow.'';
    };
  };

  config = mkIf cfg.enable {
    systemd.services.apache-airflow = {
      wantedBy = ["multi-user.target"];
      after = ["docker.service" "docker.socket"];
      environment = {
        AIRFLOW_UID="${toString cfg.uid}";
        AIRFLOW_PROJ_DIR="${cfg.dir}";
      };
    
      serviceConfig = {
        Type = "simple";
        WorkingDirectory = cfg.dir; 
        ExecStart = ''
          /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} up'
        '';
        # ExecStop = "/bin/kill -2 $MAINPID";
        ExecStop = ''
          /bin/sh -c '${pkgs.docker-compose}/bin/docker-compose -f ${dockerfile} kill'
        '';
      };
    };

    networking.firewall = let 
      ports = [] 
        ++ (if cfg.openFirewall then [ cfg.port] else []);
    in {
      allowedTCPPorts = ports;
    };
  };
}
