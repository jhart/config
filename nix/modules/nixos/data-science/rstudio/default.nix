{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.data-science.rstudio-server;
  my-rstudio = pkgs.rstudioServerWrapper.override {
    packages = with pkgs.rPackages; [
      EValue
      Hmisc
      arsenal
      cowplot
      crayon
      descr
      DescTools
      diagram
      dplyr
      extrafont
      finetune
      foreign
      fst
      furniture
      ggplot2
      ggthemes
      gt
      gtsummary
      haven
      here
      ipw
      janitor
      jsonlite
      knitr
      labelled
      languageserver
      lavaan
      ltm
      mice
      naniar
      pROC
      readxl
      readr
      rlang
      rpart
      scales
      semTools
      sjPlot
      sjmisc
      skimr
      srvyr
      stargazer
      survey
      tidymodels
      tidytidbits
      tidyverse
      vctrs
      vip
      visdat
      weights
      xml2
      xts
    ];
  };
in {
  options.knut.data-science.rstudio-server = {
    enable = mkOption {
      default = false;
      description = "Enable RStudio server";
    };
  };

  config = mkIf cfg.enable {
    services.rstudio-server = {
      enable = true;
      package = my-rstudio;
      listenAddr = "127.0.0.1";
    };

    environment.systemPackages = [
      my-rstudio
    ];
    users.users.rstudio = {
      initialPassword = "rstudio";
      createHome = true;
      isNormalUser = true;
      description = "RStudio user";
      extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
      shell = pkgs.fish;
    };
  };
}
