{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.data-science.jupyterhub;
  # pyarrow_hotfix = pkgs.python3Packages.buildPythonPackage rec {
  #   pname = "pyarrow_hotfix";
  #   version = "0.6";
  #   format = "pyproject";

  #   src = pkgs.python3Packages.fetchPypi rec {
  #     inherit pname version;
  #     sha256 = "sha256-edPgMPf/iQ1AihAKwW1vALFNRKUC14l82fw+OlNOmUU=";
  #   };

  #   propagatedBuildInputs = [
  #     pkgs.python3Packages.hatchling
  #   ];

  #   pythonImportsCheck = [
  #     "pyarrow_hotfix"
  #   ];
  # };

  env = pkgs.python311.withPackages (pythonPackages:
    with pythonPackages; [
      datafusion
      importlib-metadata
      ipykernel
      minio # Python API for accessing minio
      pandas
      polars
      py4j
      # pyarrow
      # pyarrow_hotfix
      # pyspark
      # scikit-learn
      pkgs.knut.pyballista
      pkgs.knut.deltalake
      pkgs.knut.delta-spark
    ]);
in {
  options.knut.data-science.jupyterhub = {
    enable = mkOption {
      default = false;
      description = "Enable JupyterHub service";
    };
    port = mkOption {
      default = 9833;
      description = "JupyterHub port";
    };
    openFirewall = mkOption {
      default = false;
      description = "Opens the firewall port";
    };
  };

  config = mkIf cfg.enable {
    environment.variables = {
      # PYSPARK_PYTHON = mkForce "${env}/bin/${env.executable}";
      # PYSPARK_ALLOW_INSECURE_GATEWAY = "1";
      # PYTHONPATH = "$PYTHONPATH:${pkgs.python311Packages.py4j}:${env}:${pkgs.python311Packages.pyspark}:${pkgs.spark}/python:${pkgs.spark}/python/lib/py4j-0.10.9.7-src.zip";
      JAVA_HOME = "${pkgs.jdk}";
    };
    services.jupyterhub = {
      enable = true;
      port = 14123;
      host = "0.0.0.0";
      kernels = {
        python3 = {
          displayName = "Python 3 for machine learning";
          argv = [
            "${env.interpreter}"
            "-m"
            "ipykernel_launcher"
            "-f"
            "{connection_file}"
          ];
          language = "python";
          logo32 = "${env}/${env.sitePackages}/ipykernel/resources/logo-32x32.png";
          logo64 = "${env}/${env.sitePackages}/ipykernel/resources/logo-64x64.png";
        };
      };
      # c.Spawner.environment.update({"JUPYTER_PREFER_ENV_PATH": "0","JAVA_HOME": "${pkgs.jdk}","SPARK_HOME": "${pkgs.spark}"})
      extraConfig = ''
        c.Spawner.env_keep = ['PATH', 'PYTHONPATH', 'CONDA_ROOT', 'CONDA_DEFAULT_ENV', 'VIRTUAL_ENV', 'LANG', 'LC_ALL', 'JUPYTERHUB_SINGLEUSER_APP', 'JAVA_HOME', 'SPARK_HOME']
        c.Spawner.environment = {"JAVA_HOME": "${pkgs.jdk}","SPARK_HOME": "${pkgs.spark}"}
      '';
    };

    networking.firewall = let
      ports =
        []
        ++ (
          if cfg.openFirewall
          then [cfg.port]
          else []
        );
    in {
      allowedTCPPorts = ports;
    };
  };
}
