{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.tuxedo;
in {
  options.knut.tuxedo = {
    enable = mkOption {
      default = false;
      description = "Load Tuxedo drivers";
    };
  };

  config = mkIf cfg.enable {
    boot.extraModulePackages = [
      # pkgs.knut.tuxedo-drivers
      # pkgs.linuxKernel.packages.linux_6_12.tuxedo-drivers
    ];
    hardware.tuxedo-drivers.enable = true;
    boot.kernelModules = [
      "clevo_acpi"
      "clevo_wmi"
      "ite_8291"
      "ite_8291_lb"
      "ite_8297"
      "ite_829x"
      "tuxedo_compatibility_check"
      "tuxedo_io"
      "tuxedo_keyboard"
      "tuxedo_nb02_nvidia_power_ctrl"
      "tuxedo_nb04_kbd_backlight"
      "tuxedo_nb04_keyboard"
      "tuxedo_nb04_power_profiles"
      "tuxedo_nb04_sensors"
      "tuxedo_nb04_wmi_ab"
      "tuxedo_nb04_wmi_bs"
      "tuxedo_nb05_ec"
      "tuxedo_nb05_fan_control"
      "tuxedo_nb05_kbd_backlight"
      "tuxedo_nb05_keyboard"
      "tuxedo_nb05_power_profiles"
      "tuxedo_nb05_sensors"
      "uniwill_wmi"
    ];
  };
}
