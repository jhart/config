{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.base;
in {
  options.knut.base = {
    enable = mkOption {
      default = false;
      description = "Enable base settings";
    };
  };

  config = mkIf cfg.enable {
    # Default Editor
    environment.variables.EDITOR = lib.mkForce "hx";

    environment.systemPackages = with pkgs; [
      aerc # Email client
      dua # disk space analyzer, use: dua i
      ncdu # disk space analyzer, use like ncdu -x -q
      dutree # disk space analyzer
      btop # system monitor
      alejandra # nix formatter
      superfile # file manager
      neovim # editor
      s-tui # monitor CPU power consumption
      zed-editor
      helix
      yazi # file manager
      # neovim
    ];

    # Bluetooth
    hardware.bluetooth.enable = true;
    hardware.bluetooth.powerOnBoot = true;
    hardware.bluetooth.settings = {
    };

    # Shell
    programs.fish = {
      enable = true;
    };

    # Fonts
    fonts.packages = with pkgs; [
      corefonts
      open-sans
      roboto
      roboto-mono
      vistafonts
      # (nerdfonts.override {fonts = ["FiraCode" "DroidSansMono"];})
      nerd-fonts.noto
      nerd-fonts.hack
      nerd-fonts.fira-code
      nerd-fonts.fira-mono
      nerd-fonts.roboto-mono
      nerd-fonts.inconsolata
      noto-fonts
      fira-code
      fira-code-symbols
    ];

    # Set your time zone.
    time.timeZone = lib.mkDefault "Europe/Berlin";

    # Select internationalisation properties.
    i18n.defaultLocale = lib.mkDefault "de_DE.UTF-8";

    i18n.extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
    };

    powerManagement = lib.mkDefault {
      enable = true;
      cpuFreqGovernor = "performance";
    };

    # Security
    security.sudo.enable = true;
    security.sudo.configFile = ''
      wheel ALL=(ALL:ALL) SETENV: ALL
    '';

    console = lib.mkForce {
      font = "Lat2-Terminus16";
      keyMap = "de";
    };
  };
}
