{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  rustVersion =
    pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default);
  cfg = config.knut.vscode-server;
in {
  options.knut.vscode-server = {
    enable = mkOption {
      default = false;
      description = "Enable VSCode server";
    };
    port = mkOption {
      default = 8769;
    };
  };

  config = mkIf cfg.enable {
    services.openvscode-server = {
      enable = true;
      user = "knut";
      # connectionToken = "roger";
      withoutConnectionToken = true;
      group = "users";
      userDataDir = "/home/knut/.openvscode-server";
      serverDataDir = "/var/lib/openvscode-server";
      host = "localhost";
      port = cfg.port;
      extraPackages = with pkgs; [
        (rustVersion.override {
          extensions = ["rust-src"];
          targets = ["wasm32-unknown-unknown"];
        })
        clippy
        diesel-cli
        gcc
        glibc
        keyutils
        krb5
        libGL
        libpqxx
        libxkbcommon
        openssh
        openssl
        pkg-config
        postgresql
        rust-analyzer
        rustc
        sass
      ];
      extraEnvironment = {
        PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
        # PKG_CONFIG_PATH = "/run/current-system/sw/lib/pkgconfig";
        # RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;
      };
      extensionsDir = "/home/knut/.vscode-server-extensions";
    };
  };
}
