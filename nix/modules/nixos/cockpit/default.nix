{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.cockpit;
  my-python-packages = ps:
    with ps; [
      pygobject3
      libvirt
      # other python packages
    ];
in {
  options.knut.cockpit = {
    enable = mkOption {
      default = false;
      description = "Enable cockpit";
    };
    port = mkOption {
      default = 9090;
    };
  };

  config = mkIf cfg.enable {
    services.cockpit = rec {
      enable = true;
      port = cfg.port;
      settings = {
        WebService = {
          AllowUnencrypted = true;
        };
      };
    };
    virtualisation.libvirtd = {
      enable = true;
    };
    environment.systemPackages = with pkgs; [
      knut.cockpit-machines # the machines plugin
      virt-manager # for creating new machines
      libosinfo # for filling the os dropdown
      osinfo-db # for filling the os dropdown
      libvirt
      libvirt-glib
      # (pkgs.python311Full.withPackages my-python-packages)
    ];
  };
}
