{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.networking.wireguard.server_dns;
  wg_subnet = "10.66.66";
  wg_interface_int = "wg0";
  wg_interface_ext = "enp0s31f6";
  wg_port = 9176;
in {
  options.knut.networking.wireguard.server_dns = {
    enable = mkOption {
      default = false;
      description = "Enable wireguard server";
    };
    subnet = mkOption {
      default = "10.66.66";
      type = types.str;
      description = "The subnet";
    };
  };

  config = mkIf cfg.enable {
    # Enable NAT
    networking.nat = {
      enable = true;
      enableIPv6 = false;
      externalInterface = wg_interface_ext;
      internalInterfaces = [wg_interface_int];
    };

    # Open ports in the firewall
    networking.firewall = {allowedUDPPorts = [wg_port];};

    networking.wg-quick.interfaces = {
      wg0 = {
        address = ["${cfg.subnet}.1/24"];
        listenPort = 9176;
        privateKeyFile = config.sops.secrets.wireguard_jonne_private_key.path;

        peers = [
          {
            # peer0
            publicKey =
              builtins.readFile
              config.sops.secrets.wireguard_roger_public_key.path;
            allowedIPs = ["${cfg.subnet}.2/32" ];
          }
          {
            # peer0
            publicKey =
              builtins.readFile
              config.sops.secrets.wireguard_fairphone_public_key.path;
            allowedIPs = ["${cfg.subnet}.3/32" ];
          }
          {
            # peer0
            publicKey =
              builtins.readFile
              config.sops.secrets.wireguard_tablet_public_key.path;
            allowedIPs = ["${cfg.subnet}.4/32"];
          }
        ];

        postUp = ''
          ${pkgs.iptables}/bin/iptables -A FORWARD -i ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s ${cfg.subnet}.1/24 -o ${wg_interface_ext} -j MASQUERADE
          # ${pkgs.iptables}/bin/ip6tables -A FORWARD -i ${wg_interface_int} -j ACCEPT
          # ${pkgs.iptables}/bin/ip6tables -t nat -A POSTROUTING -s fdc9:281f:04d7:9ee9::1/64 -o ${wg_interface_ext} -j MASQUERADE
        '';

        # Undo the above
        preDown = ''
          ${pkgs.iptables}/bin/iptables -D FORWARD -i ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s ${cfg.subnet}.1/24 -o ${wg_interface_ext} -j MASQUERADE
          # ${pkgs.iptables}/bin/ip6tables -D FORWARD -i ${wg_interface_int} -j ACCEPT
          # ${pkgs.iptables}/bin/ip6tables -t nat -D POSTROUTING -s fdc9:281f:04d7:9ee9::1/64 -o ${wg_interface_ext} -j MASQUERADE
        '';
      };
    };
  };
}
