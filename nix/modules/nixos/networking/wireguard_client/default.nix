{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.networking.wireguard_client;
  wg_port = 9176;
  instance = name: cfg2:
    lib.mkIf cfg2.enable {
      address = ["${cfg2.subnet}.2/24"];
      privateKeyFile = config.sops.secrets.wireguard_roger_private_key.path;

      peers = [
        {
          publicKey =
            builtins.readFile
            config.sops.secrets.wireguard_jonne_public_key.path;
          allowedIPs = ["${cfg2.subnet}.1/32"];
          endpoint = "${cfg2.endpoint}:${builtins.toString wg_port}";
          persistentKeepalive = 10;
        }
      ];
    };
in {
  options.knut.networking.wireguard_client.enable = mkOption {
    default = false;
    description = "Enable wireguard clients";
  };
  options.knut.networking.wireguard_client.peers = mkOption {
    description = ''
      Wireguard client
    '';
    type = types.attrsOf (types.submodule {
      options = {
        enable = mkOption {
          default = false;
          description = "Enable wireguard client";
        };
        subnet = mkOption {
          type = types.str;
          description = "The subnet";
        };
        endpoint = mkOption {
          type = types.str;
          description = "The wireguard endpoint";
        };
      };
    });
  };

  config = with lib; let
  in
    mkIf cfg.enable {
      #   notEmpty = head (attrsets.attrValues cfg) != [];
      # in
      #   mkIf cfg.enable {
      networking.firewall.allowedUDPPorts = [wg_port];
      networking.firewall.checkReversePath = "loose";
      networking.resolvconf.enable = true;

      networking.wg-quick.interfaces = lib.mapAttrs instance cfg.peers;
    };
}
/*
Example:
       mapAttrsToList (name: value: name + value)
          { x = "a"; y = "b"; }
       => [ "xa" "yb" ]

       builtins.catAttrs "a" [{a = 1;} {b = 0;} {a = 2;}]
       => [1 2]
*/

