{
  lib,
  pkgs,
  inputs,
  system, # The system architecture for this host (eg. `x86_64-linux`).
  target, # The Snowfall Lib target for this system (eg. `x86_64-iso`).
  format, # A normalized name for the system target (eg. `iso`).
  virtual, # A boolean to determine whether this system is a virtual target using nixos-generators.
  systems, # An attribute map of your defined hosts.
  config,
  options,
  ...
}:
with lib; let
  cfg = config.knut.networking.wireguard.server;
  wg_subnet = "10.100.0";
  wg_interface_int = "wg0";
  wg_interface_ext = "enp0s31f6";
  wg_port = 9176;
in {
  options.knut.networking.wireguard.server = {
    enable = mkOption {
      default = false;
      description = "Enable wireguard server";
    };
  };

  config = mkIf cfg.enable {
    networking.nat.enable = true;
    networking.nat.externalInterface = wg_interface_ext;
    networking.nat.internalInterfaces = [wg_interface_int];
    networking.firewall = {
      trustedInterfaces = [wg_interface_int];
      allowedUDPPorts = [wg_port];
      allowedTCPPorts = [80 443 22 wg_port];
    };

    networking.wireguard.interfaces = {
      "${wg_interface_int}" = {
        ips = ["${wg_subnet}.1/24"];

        listenPort = wg_port;

        postSetup = ''
          ${pkgs.iptables}/bin/iptables -t nat -I POSTROUTING 1 -s ${wg_subnet}.0/24 -o ${wg_interface_ext} -j MASQUERADE
          ${pkgs.iptables}/bin/iptables -I INPUT 1 -i ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -I FORWARD 1 -i ${wg_interface_ext} -o ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -I FORWARD 1 -i ${wg_interface_int} -o ${wg_interface_ext} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -I INPUT 1 -i ${wg_interface_ext} -p udp --dport ${
            builtins.toString wg_port
          } -j ACCEPT
          ${pkgs.sysctl}/bin/sysctl net.ipv4.ip_forward=1
          ${pkgs.sysctl}/bin/sysctl net.ipv4.conf.all.proxy_arp=1
        '';

        postShutdown = ''
          ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s ${wg_subnet}.0/24 -o ${wg_interface_ext} -j MASQUERADE
          ${pkgs.iptables}/bin/iptables -D INPUT -i ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -D FORWARD -i ${wg_interface_ext} -o ${wg_interface_int} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -D FORWARD -i ${wg_interface_int} -o ${wg_interface_ext} -j ACCEPT
          ${pkgs.iptables}/bin/iptables -D INPUT -i ${wg_interface_ext} -p udp --dport ${
            builtins.toString wg_port
          } -j ACCEPT
        '';

        privateKeyFile = config.sops.secrets.wireguard_jonne_private_key.path;

        peers = [
          {
            publicKey =
              builtins.readFile
              config.sops.secrets.wireguard_roger_public_key.path;
            allowedIPs = ["${wg_subnet}.2/32" "0.0.0.0/24"];
            name = "roger";
            persistentKeepalive = 10;
          }
          {
            publicKey =
              builtins.readFile
              config.sops.secrets.wireguard_fairphone_public_key.path;
            allowedIPs = ["${wg_subnet}.2/32" "0.0.0.0/24"];
            name = "fairphone";
            persistentKeepalive = 10;
          }
        ];
      };
    };
  };
}
# Check UDP reception: sudo tcpdump -i lo udp -> if yes, its is the firewall or wireguard itself

