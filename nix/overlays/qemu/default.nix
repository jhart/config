{
  channels,
  nixgl,
  ...
}: final: prev: {
  # For example, to pull a package from unstable NixPkgs make sure you have the
  # input `unstable = "github:nixos/nixpkgs/nixos-unstable"` in your flake.
  # inherit (channels.unstable) chromium;

  qemu = prev.qemu.override {smbdSupport = true;};
}
