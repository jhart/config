{
  docker = {
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };
  podman.enable = true;

  # https://mynixos.com/options/virtualisation.oci-containers.containers.%3Cname%3E
  oci-containers = {
    backend = "podman";

    containers = {
      hello = {
        autoStart = true;
        image = "strm/helloworld-http";
        ports = ["80:80"];
      };
    };
  };
}
