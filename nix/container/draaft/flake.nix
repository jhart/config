# Try: https://noqqe.de/blog/2021/02/06/nextcloud-mit-nixos-containers/
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    draaft.url = "git+http://jonne:8929/knut/draaft?branch=staging";
  };

  outputs = {
    self,
    nixpkgs,
    draaft,
  }: {
    nixosConfigurations.container = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";

      modules = [
        ({pkgs, ...}: (import ./default.nix {
          inherit pkgs;
          backend = draaft.packages.x86_64-linux.backend;
          frontend = draaft.packages.x86_64-linux.frontend;
        }))
      ];
    };
  };
}
# sudo nixos-container create cix --flake flake.nix
# sudo nixos-container start cix
# curl http://cix

