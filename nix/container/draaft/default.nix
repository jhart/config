{
  pkgs,
  backend,
  frontend,
  ...
}: let
  db_user = "knut";
  app_url = "localhost";
  app_port = "4100";
  cli_port = "8000";
  database_pool_size = "2";
  database_name = "draaft";
  database_url = "postgresql://${db_user}:${db_user}@localhost/${database_name}";
  graphql_endpoint = "/graphql";
  environment = "development";
  stripe_key = "sk_test_51O4RpvAB4F9jEZl22Tvs1eIyuDwPy50XvxRswKUzjrwM2e1eThBlQokoA76vJ1qJwxhNx4V9qtUV6VmKp13H86oa00pEORvNvM";
in {
  boot.isContainer = true;
  system.stateVersion = "24.05";

  environment.systemPackages = with pkgs; [diesel-cli backend frontend git];

  environment.variables = {
    APP_URL = app_url;
    APP_PORT = app_port;
    DATABASE_POOL_SIZE = database_pool_size;
    DATABASE_URL = database_url;
    ENVIRONMENT = environment;
    GRAPHQL_ENDPOINT = graphql_endpoint;
    EMAIL_HOST = "smtps.udag.de";
    EMAIL_HOST_PASSWORD = "draaft777#";
    EMAIL_HOST_USER = "info@draaft.co";
    EMAIL_PORT = "587";
    STRIPE_KEY = stripe_key;
  };

  # Disable DHCP and firewall
  networking.useDHCP = false;
  networking.firewall.enable = false;

  # Use systemd resolver, otherwise it does not work
  networking.useHostResolvConf = false;
  services.resolved.enable = true;

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts."draaft" = {
      locations."/" = {
        proxyPass = "http://127.0.0.1:${cli_port}";
        proxyWebsockets = true;
        extraConfig =
          "proxy_ssl_server_name on;"
          + "proxy_pass_header Authorization;";
      };
      locations."/graphql" = {
        proxyPass = "http://127.0.0.1:${app_port}";
        proxyWebsockets = true;
        extraConfig =
          "proxy_ssl_server_name on;"
          + "proxy_pass_header Authorization;";
      };
    };
  };

  services.redis.servers = {
    draaft = {
      enable = true;
      databases = 16;
      logLevel = "debug";
      port = 6379;
      bind = "0.0.0.0";
      extraParams = ["--protected-mode no"];
    };
  };

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql;
    port = 5432;
    ensureDatabases = [db_user];
    ensureUsers = [
      {
        name = "${db_user}";
        ensureDBOwnership = true;
      }
    ];
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all all trust
      host all all 127.0.0.1/32 trust
      host all all ::1/128 trust
      host  all  all 0.0.0.0/0 md5
    '';

    # initialScript = pkgs.writeText "initScript" ''
    #   CREATE ROLE ${db_user} WITH LOGIN PASSWORD '${db_user}' SUPERUSER CREATEDB CREATEROLE REPLICATION BYPASSRLS;
    #   CREATE DATABASE draaft;
    #   GRANT ALL PRIVILEGES ON DATABASE draaft TO ${db_user};
    #   ALTER USER postgres PASSWORD 'postgres';
    # '';
  };

  systemd.services.draaft-backend = {
    description = "Draaft backend service";
    after = ["network.target" "postgresql.service" "redis-draaft.service"];
    wantedBy = ["multi-user.target"];
    # preStart = ''
    #   ${backend}/bin/backend seed
    # '';
    serviceConfig = {
      ExecStart = "${backend}/bin/backend";
      User = "root";
      Group = "root";
      Restart = "always";
      RestartSec = "5s";
      Environment = [
        "APP_URL=${app_url}"
        "APP_PORT=${app_port}"
        "DATABASE_POOL_SIZE=${database_pool_size}"
        "DATABASE_URL=${database_url}"
        "ENVIRONMENT=${environment}"
        "GRAPHQL_ENDPOINT=${graphql_endpoint}"
        "STRIPE_KEY=${stripe_key}"
      ];
    };
  };

  systemd.services.draaft-frontend = {
    description = "Draaft frontend";
    after = ["network.target" "draaft-backend.service"];
    wantedBy = ["multi-user.target"];
    serviceConfig = {
      ExecStart = "${frontend}/bin/frontend";
      User = "root";
      Group = "root";
      Restart = "always";
      RestartSec = "5s";
      Environment = [
        "APP_URL=${app_url}"
        "APP_PORT=${app_port}"
        "ENVIRONMENT=${environment}"
        "GRAPHQL_ENDPOINT=${graphql_endpoint}"
      ];
    };
  };
}
