# https://jacobneplokh.com/how-to-setup-nextcloud-on-nixos/
{
  config,
  pkgs,
  ...
}: let
  dbuser = "nextcloud";
  dbpwd = "test123";
  dbname = "nextcloud";
in {
  # Actual Nextcloud Config
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud27;
    hostName = "nc.jhartma.org";

    # Use HTTPS for links
    https = true;

    # Auto-update Nextcloud Apps
    autoUpdateApps.enable = true;
    # Set what time makes sense for you
    autoUpdateApps.startAt = "05:00:00";

    config = {
      # Further forces Nextcloud to use HTTPS
      overwriteProtocol = "https";

      # Nextcloud PostegreSQL database configuration, recommended over using SQLite
      dbtype = "pgsql";
      dbuser = dbuser;
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = dbname;
      dbpassFile = "${pkgs.writeText "${dbuser}" "${dbpwd}"}";
      adminpassFile = "${pkgs.writeText "${dbuser}" "${dbpwd}"}";
      adminuser = dbuser;
    };
  };

  # Enable PostgreSQL
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_16;

    # Ensure the database, user, and permissions always exist
    ensureDatabases = [dbname dbuser];
    ensureUsers = [
      {
        name = dbuser;
        ensureDBOwnership = true;
      }
    ];
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all all trust
      host all all 127.0.0.1/32 trust
      host all all ::1/128 trust
      host  all  all 0.0.0.0/0 md5
    '';
    # initialScript = pkgs.writeText "initScript" ''
    #   CREATE ROLE ${dbuser} WITH LOGIN PASSWORD '${dbpwd}' SUPERUSER CREATEDB CREATEROLE REPLICATION BYPASSRLS;
    #   CREATE DATABASE ${dbname};
    #   GRANT ALL PRIVILEGES ON DATABASE ${dbname} TO ${dbuser};
    #   ALTER USER postgres PASSWORD 'postgres';
    # '';
  };

  # Ensure that postgres is running before running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };
}
