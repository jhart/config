# Try: https://noqqe.de/blog/2021/02/06/nextcloud-mit-nixos-containers/
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
  }: {
    nixosConfigurations.container = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";

      modules = [
        ({pkgs, ...}: {
          boot.isContainer = true;
          system.stateVersion = "24.05";

          # Let 'nixos-version --json' know about the Git revision
          # of this flake.
          system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

          # Network configuration.
          networking.useDHCP = false;
          networking.firewall = {
            enable = true;
            allowedTCPPorts = [80 443];
          };

          # Agree to Let's Encrypt Terms
          security.acme = {
            acceptTerms = true;
            defaults.email = "joerghartmann@mailbox.org";
          };

          imports = [./nextcloud.nix];

          environment.systemPackages = with pkgs; [curl postgresql];

          programs.fish.enable = true;

          users.users.knut = {
            isNormalUser = true;
            home = "/home/knut";
            extraGroups = ["wheel" "networkmanager" "docker" "audio" "video" "libvirtd"];
            uid = 1000;
            shell = pkgs.fish;
            initialPassword = "knut";
          };
        })
      ];
    };
  };
}
# sudo nixos-container create cix --flake flake.nix
# sudo nixos-container start cix
# curl http://cix

