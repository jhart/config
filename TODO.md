# Bente Homeserver
## Specs:
- AMD Ryzen 9 7950X
- 96GB Memory
- 4TB Raid storage

## TODO
- reinstall windows vm
- allow other users, e.g. Kristofer, safely
- Systemd service for Windows VM

## Implemented
- Code organization
  - use nixpkgs stable for servers
  - use nixpkgs unstable for desktops
- Filesystems:
  - ZFS
  - backup zfs drives to external disk automatically
- Theming:
  - global theming
- Network
  - SSH login
  - Wireguard VPN server
  - local dns resolver
  - custom dns servers
  - Remote RDP login
  - Shared NFS storage (/pool-bente/nfs/shared)
- Services:
  - Gitlab
  - Nextcloud
  - Mattermost
  - Nginx Reverse Proxy
- Virtual Machines
  - Fiete (Nixos)
  - Ubuntu
- Secrets management (SOPS)
-

# Roger Laptop
[[#Specs:]]
